<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mobilesoft.com</title>
    <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
    <link rel="stylesheet" href="styles/fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="styles/styles.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="styles/desktop.css" />
</head>

<body>
    <?php include_once('includes/header.php'); ?>

    <div class="container-fluid">
        <div class="row white-bg top-about">
            <!--Split left right-->
            
            <div class="light-grey-bg col-6 m-0 px-1 px-sm-5 py-2 py-sm-5">
                <div class="about-top-text">
                <h1 style="font-size:4rem;" class="center">This Is Us</h2>
                <p><span class="bold">Mobilesoft&trade; is a global leader in mobile marketing and application development. </span>We work with businesses to successfully launch
            their Android and iPhone application, drive downloads, and create new revenue channels.</p>
            <br>
            <p>Mobilesoft&trade; owns and operates a mobile application platform that launches apps on 100 million smartphones per month around the world.
                We know technology and love what we do. </p>
                <br>
                <hr>
                <div class="row">
                <img src="img/top-location-icon.png" alt="" class="col-3 col-md-5 col-sm-7" style="max-width: 32% !important;">
                <p class="col">We have over 120 million smartphone users worldwide that access our platform monthly. Mobilesoft&trade; is headquartered in mineapolis, with offices in <span class="bold">Las Vegas, Atlanta, Houston, Seattle, Chislnau, and Moldova.</span></p>
                </div>
</div>
            </div>
            <div class="contact-bg col-6 img-fluid">
                <!-- <img src="img/about-us-bg.jpg" alt="" class="img-fluid "> -->
            </div>
            
        </div>
        <!-- <div class="row light-blue-bg"> -->
        <!-- <div class="white-box philosophy-box pl-3 col-4">
                <img src="img/engage-icon.png" alt="" style="width: 20%;">
                <h3>Engage.</h3>
                <hr>
                <p class="pl-3">Why are you in business? Whatever your reason is, everyone in business must
                    master the ability to engage the customer.
                </p>
                <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">Read more</button>
            </div> -->
        <!--PHILOSOPHY SECTION-->
        <div class="light-blue-bg">
            <h2 class="white pt-5 px-5">Our Philosophy</h2>
            <div class="d-flex align-items-center justify-content-between px-4 philosophy-section">

                <div class="px-1 px-md-3 py-4 white-box philosophy-box"><img src="img/engage-icon.png" alt=""
                        style="width: 20%;">
                    <h3>Engage.</h3>
                    <hr>
                    <p class="pl-3">Why are you in business? Whatever your reason is, everyone in business must
                        master the ability to engage the customer.
                    </p>
                    <button onclick="orderLink()" class="btn btn-secondary blurple-bg text-left">READ MORE</button>
                </div>
                <div class="px-1 px-md-3 py-4 m-5 white-box philosophy-box"><img src="img/engage-icon.png" alt=""
                        style="width: 20%;">
                    <h3>Engage.</h3>
                    <hr>
                    <p class="pl-3">Why are you in business? Whatever your reason is, everyone in business must
                        master the ability to engage the customer.
                    </p>
                    <button onclick="orderLink()" class="btn btn-secondary blurple-bg text-left">READ MORE</button>
                </div>
                <div class="px-1 px-md-3 py-4 white-box philosophy-box"><img src="img/engage-icon.png" alt=""
                        style="width: 20%;">
                    <h3>Engage.</h3>
                    <hr>
                    <p class="pl-3">Why are you in business? Whatever your reason is, everyone in business must
                        master the ability to engage the customer.
                    </p>
                    <button onclick="orderLink()" class="btn btn-secondary blurple-bg text-left">READ MORE</button>
                </div>

            </div>
        </div>
        <!--END OF PHILOSOPHY SECTION-->
        <div class="map">
            <div class="map-text text-center">
                <h3 class="bold">We're On The Map <br>
                    Let us do the same for you.
                </h3>
                <p class="white">Mobilesoft&trade; headquartered in Minneapolis, with offices in Las Vegas, Seattle,
                    Houston, and
                    Chisinau. We live and breathe mobile every day and love what we do.</p>
            </div>
        </div>
        <div class="mission mid-grey-bg row">
        
            <div class="mission-text col-6">
                <h1 style="font-size: 4rem;">Our Mission Statement</h1>
                <h3 class="light-blue bold">(Because we believe businesses should live by one)</h3>
                <br>
                <br>
                <p class="bold">To manage an innovative ecosystem that allows our clients to engage, interact, and
                    retain their
                    markets by use of mobile app technology.</p>
                    <br>
                    <br>
                <p>Most importantly, allow that ecosystem to be managed by passionate employees, engaged
                    customers, and
                    visionary shareholders that understand the positive impact they can make in the world of business
                </p>
            </div>

        </div>
        <div id="counter" class="md-blue-grad">
            <div class="row text-center pl-5">
                <div class="col-3">
                    <div class="row"><div class="counter-value" data-count="75">0</div>
                    <span class="counter-value">%+</span></div>
                    
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
                <div class="col-3">
                <div class="row"><div class="counter-value" data-count="90">0</div>

                    <span class="counter-value">%+</span></div>
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
                <div class="col-3">
                <div class="row"><div class="counter-value" data-count="22">0</div>
                    <span class="counter-value">+</span></div>
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
                <div class="col-3">
                <div class="row"><div class="counter-value" data-count="13">0</div>
                    <span class="counter-value">+</span></div>
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
            </div>
            <div class="row text-center pl-5">
                <div class="col-3">
                    <div class="row"><div class="counter-value" data-count="80">0</div>
                    <span class="counter-value">+</span></div>
                    
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
                <div class="col-3">
                <div class="row"><div class="counter-value" data-count="400">0</div>

                    <span class="counter-value">+</span></div>
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
                <div class="col-3">
                <div class="row"><div class="counter-value" data-count="18">0</div>
                    <span class="counter-value">+</span></div>
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
                <div class="col-3">
                <div class="row"><div class="counter-value" data-count="0">0</div>
                    <span class="counter-value">+</span></div>
                    <p class="subline">Percentage of employees who have worked for a fortune 500 company</p>
                </div>
            </div>
    
        </div>
        <div class="content"></div>
        <div class="d-flex align-items-center justify-content-between px-4 marketing-section">


            <div class="px-1 px-md-3 py-4 m-5 white-box marketing-box">
                <h2 class="white">Marketing Agencies</h2>
                <hr>
                <p class="pl-3 bold">The competititon is fierce in the world of interactive marketing. You have a
                    successful
                    business model, with an effective product and service portfolio.
                </p>
                <br>
                <p class="pl-3">Now it's time to add native Android and iPhone apps to your product mix. The
                    mobilesoft
                    partner
                    program is designed to allow your organization to start publishing mobile apps with in the first
                    month.</p>
                <img src="img/ama-logo.png" alt="" class="pl-3">
                <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">READ MORE</button>
            </div>
            <div class="px-1 px-md-3 py-4 white-box marketing-box">
                <h3 class="white">Technology. Stacked.</h3>
                <hr>
                <p class="pl-3">Mobilesoft&trade; is a proud Preferred Partner designation of Amazon Web Services&reg;.

                </p>
                <p class="pl-3">We utilizes the Amazon Web Services&reg; (AWS) infrastructure as a service platform to
                    provide all of our clients a modern, security-resilent and fully scalable server and hosting
                    enviroment.
                </p>
                <img src="img/aws-logo.png" alt="">
                <button onclick="orderLink()" class="btn btn-secondary blurple-bg text-left">READ MORE</button>
            </div>

        </div>
        <div class="general-manager center row">
            <img src="img/jason-sato-img.png" alt="" class="col-4">
            <p class="col-8">It's refreshing to be a part of a technology company that fosters employee empowerment allowing us to do what is best for our customer and the company we are grown to love</p>
            <p><span class="bold light-blue">Jason Soto</span></p>
        </div>
    </div>
    <?php include_once('includes/footer.php'); ?>
    <script src="js/vendor/jquery-3.3.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script>
    var a = 0;
$(window).scroll(function() {

  var oTop = $('#counter').offset().top - window.innerHeight;
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.counter-value').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },

        {

          duration: 6500,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});
    </script>
</body>

</html>