<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mobilesoft.com</title>
  <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="styles/vendor/slick.css" />
  <link rel="stylesheet" type="text/css" href="styles/vendor/slick-theme.css" />
  <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
  <link rel="stylesheet" href="styles/fonts.css">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="styles/styles.css" />
  <link rel="stylesheet" type="text/css" href="styles/vendor/plyr.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="styles/industry.css" />
</head>

<body>
  <?php include_once('includes/header.php'); ?>

  <div class="container-fluid">
    <!--Split left right-->
    <div class="row bail-upper">
      <div class="col-4"></div>
      <div class="col-4 text-center">
        <img class="img-fluid pt-lg-5 pt-3 pt-md-4 pb-lg-5 pb-3 pb-md-4 mb-lg-3" src="img\top-banner-iphone-food.png"
          alt="">
      </div>
      <div class="col-4 my-auto">
        <h1 class="text-left">Get your<br>Mobile app<br>for your Restaurant</h1>
      </div>
    </div>
  </div>

  

  <div class="row">
    <div class="text-center pt-3 video-bg m-0 p-0">
      <h2 class="text-white">Restaurants Apps</h2>
      <p class="text-white">The top restaurant brands in the world are showing increased profits and customer retention from the launch of their own mobile app. It's time you joined the club</p>
      <div class="video-frame">
        <video poster="/img/bail-video-cover.jpg" id="player" playsinline controls>
          <source src="/video/mobilesoft-bail-app-video.mp4" type="video/mp4" />
        </video>
      </div>
      <div class="play-video-text">
        <img class="img-fluid" src="/img/play-video-label.png" alt="">
      </div>
    </div>
  </div>

  <div class="row blurple-bg">
    <div class="d-flex mb-0 mt-2 py-0 app-benefits">
      <img class="img-fluid align-self-end" src="/img/iphone-food.png" alt="">
      <div class="align-self-start benefit pl-5">
        <h2 class="light-blue bold">Your Restaurant. Your App.</h2>
        <h4 class="white">We are the #1 mobile app builder in the Restaurants and Food Industry</h4>
        <p>Don't allow your restaurant to get lost in the sea of "foodie apps". Let your customers connect directly with your brand. They love your food. Give them the tools to keep coming back.</p>
      </div>
    </div>
  </div>



  <div class="md-blue-grad">
    <div class="bail-selector my-5">
      <div class="text-center mb-0">
        <h3>APP FEATURES</h3>
      </div>
      <div class="d-flex">
        <div class="text-center e-sign pr-2">
          <span id="e-sign-bail" onclick="industrySelector()">
            <img class="img-fluid w-50" src="img/ordering.png" alt="" id="mobile-ordering"
              onclick="industrySelector()">
            <h2 class="light-blue">Mobile Ordering</h2>
            <div class="light-grey">Allow your customers to order ahead to save time and allow for you to increase sales and create customer loyalty by using your mobile app (and not the big guys).
            </div>
          </span>
        </div>
        <div class="text-center phone">
          <img id="frame" class="img-fluid" src="img/feature-loyalty.png" alt="">
        </div>
        <div class="text-center mobile-payments pl-2">
          <span id="mobile-payments-bail" onclick="industrySelector()">
            <img class="img-fluid w-50" src="img/button-mobile-payments.png" alt="" id="loyalty-coupons-restaurant"
              onclick="industrySelector()">
            <h2 class="light-blue">Coupons & Loyalty</h2>
            <div class="light-grey">Offer your customers with in-app mobile coupons, rewarding your customers with special offers for downloading your mobile app.
            </div>
          </span>
        </div>
      </div>
    </div>
    <div class="special-features text-center">
      <h1 class="col light-blue mb-4">Mobile Apps Do The Work</h1>
      <div class="row">
        <div class="col white-box text-center">
          <img src="img/icon-promotional-message.png" alt="">
          <p class="bold">Promotional Messages</p>
          <p class="light-grey">
            Update your app in real-time with new content and products.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-stamp-card.png" alt="">
          <p class="bold">Stamp Card</p>
          <p class="light-grey">
          Update your app in real-time with new content and products.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-custom-coupons.png" alt="">
          <p class="bold">Custom Coupons</p>
          <p class="light-grey">
          Update your app in real-time with new content and products.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-mobile-menu.png" alt="">
          <p class="bold">Mobile Menu</p>
          <p class="light-grey">
          Update your app in real-time with new content and products.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col white-box text-center">
          <img src="img/icon-location-message.png" alt="">
          <p class="bold">Location Based Messages</p>
          <p class="light-grey">
          Update your app in real-time with new content and products.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-payments.png" alt="">
          <p class="bold">Payments</p>
          <p class="light-grey">
          Update your app in real-time with new content and products.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-social-media.png" alt="">
          <p class="bold">Social Media</p>
          <p class="light-grey">
          Update your app in real-time with new content and products.

          </p>
        </div>
        <div class="col white-box text-center">
          <img src="" alt="">
          <p class="bold">Share App</p>
          <p class="light-grey">
            Update your app in real-time with new content and products.
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="d-flex banner-bail testimonial justify-content-between my-0 py-0">
    <div class="align-self-end app-preview">
    <img class="img-fluid px-lg-3 px-md-3 px-1"
        src="img/testimonial-phones-food.png"></div>
    <div class="text-center quote align-self-center py-3">
      <div>
        <span class="outer-line align-middle"></span>
        <i style="font-size: 50px;" class="fas fa-quote-left light-blue text-center align-middle px-2"></i>
        <span class="outer-line align-middle"></span>
      </div>
      <div class="white pt-3">Technology has become a game changer in the restaurant industry. The guys experience is everything and trying to replicate that into the digital space is now a reality and I am excited to be part of it. Echo & Rig chose to launch a mobile app because technology has become a game changer...
      </div>
      <div class="light-blue bold pt-4">Chef Sam Marvin</div>
      <div class="white">Echo & Rig Butcher & Steakhouse</div>
    </div>
    <div class="align-self-end avatar"><img class="img-fluid pl-lg-2" src="img/testimonial-person-food.png">
    </div>
  </div>


  <div class="white-bg my-5 py-3">
    <div class="text-center mb-4">
      <h4 class="light-blue">Some of our Loyal Customers</h4>
    </div>
    <div class="d-flex justify-content-center">
      <div class="customer p-4 border">Client 1</div>
      <div class="customer p-4 border border-left-0">Client 2</div>
      <div class="customer p-4 border border-left-0">Client 3</div>
      <div class="customer p-4 border border-left-0">Client 4</div>
      <div class="customer p-4 border border-left-0">Client 5</div>
      <div class="customer p-4 border border-left-0">Client 6</div>
    </div>
  </div>

  <?php include_once('includes/footer.php'); ?>
  <script src="js/vendor/jquery-3.3.1.min.js"></script>
  <script src="js/vendor/popper.min.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/vendor/plyr.js"></script>
  <script>
  const player = new Plyr('#player');
  </script>
</body>

</html>