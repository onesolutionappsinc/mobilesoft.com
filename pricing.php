<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
  <title>Mobilesoft.com</title>
  <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
  <link rel="stylesheet" href="styles/fonts.css">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="styles/styles.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="styles/desktop.css" />
</head>

<body>
  <?php include_once('includes/header.php'); ?>
  <div class="container-fluid content">
    <div class="container moveDown">
      <div class="headers">
        <h1 class="text-center">No Contract. Straight Foward. Clear Pricing</h1>
        <p class="grey text-center">DESIGN. BUILD. PUBLISH. YOUR MOBILE APP </p>

        <div class="col-12 text-center">
          <p class="grey text-center">on</p>
          <img src="img/play-button.png" alt="">
          <img src="img/ios-button.png" alt="">
          <img src="img/amazon-button.png" alt="">
        </div>
        <button onclick="orderLink()" class="btn btn-secondary blurple-bg center moveDown">BUILD YOUR APP NOW </button>
      </div>
      <div class="row moveDown">
        <div class="pricing-box white-box col">
          <div class=" text-center">
            <p class="grey">EASY SET UP</p>
            <h1 style="font-size: 8rem;" class="slimmer">$199</h1>
          </div>
          <hr>
          <ul class="grey">
            <li><i class="fas fa-check"></i>Complete Custom Design</li>
            <li><i class="fas fa-check"></i>Custom Feature Set</li>
            <li><i class="fas fa-check"></i>Industry Specific Tools</li>
            <li><i class="fas fa-check"></i>Live Customer Support Team</li>
            <li><i class="fas fa-check"></i>Dedicated Mobile Expert Rep</li>
            <li><i class="fas fa-check"></i>Marketing Dashboard</li>
            <li><i class="fas fa-check"></i>Enhanced Analytics<p>Listed on: <br /> Google Play Store <br>
                Apple
                App Store <br /> Website
                Direct Download</p>
            </li>
            <li><i class="fas fa-check"></i>One-Time Easy Set Up Fee</li>
          </ul>
          <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">GET STARTED
            <span>&#8594;</span></button>


        </div>
        <div class="col-2"></div>
        <div class="pricing-box white-box col grey">
          <div class="text-center ">
            <p class="">NO CONTRACT SUBSCRIPTION</p>
            <h1 style="font-size: 8rem;" class="slimmer">$79<span class="month">/mo</span>
            </h1>
          </div>
          <hr>
          <ul>
            <li><i class="fas fa-check"></i>Unlimited Push Notifications</li>
            <li><i class="fas fa-check"></i>Unlimited Customer Downloads</li>
            <li><i class="fas fa-check"></i>Location-Based (GEO) Messaging</li>
            <li><i class="fas fa-check"></i>Free Android & IOS Upgrades</li>
            <li><i class="fas fa-check"></i>Integrated Maps</li>
            <li><i class="fas fa-check"></i>Camera/Mic Features</li>
            <li><i class="fas fa-check"></i>Social Media</li>
            <li><i class="fas fa-check"></i>Reviews</li>
            <li><i class="fas fa-check"></i>Coupons & Loyalty Program</li>
            <li><i class="fas fa-check"></i>Enhanced Analytics & Reporting</li>
            <li><i class="fas fa-check"></i>Live Customer Support Team</li>
            <li><i class="fas fa-check"></i>Dedicated Mobile Expert Rep</li>
            <li><i class="fas fa-check"></i>In-App Advertising+</li>
            <li><i class="fas fa-check"></i>Mobile Payments* ($5/mo)</li>
            <li><i class="fas fa-check"></i>eSign*($20/mo)</li>
          </ul>
          <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">GET STARTED
            <span>&#8594;</span></button>
        </div>
      </div>
      <div class="row moveDown">
        <div class="pricing-box white-box col-5" style="margin: -10% 0 0">
          <div class="text-center">
            <p class="grey">YOU WIN. WE WIN. CAMPAIGN</p>
            <h1 style="font-size: 8rem;" class="slimmer">$39<span class="month grey">/mo</span>
            </h1>
            <h3 class="light-blue bold">INTRODUCTORY OFFER</h3>
          </div>
          <hr>
          <ul class="grey">
            <li><i class="fas fa-check"></i>Let our dedicated Campaign Managers promote your mobile app!
            </li>

            <li><i class="fas fa-check"></i>Increase Google Rankings
            </li>

            <li><i class="fas fa-check"></i>Promotions & Announcements
            </li>
            <li><i class="fas fa-check"></i>In-App Marketing Messages</li>
            <li><i class="fas fa-check"></i>Print Kit Materials for your location</li>
            <li><i class="fas fa-check"></i>Social Media Promotions</li>
            <li><i class="fas fa-check"></i>Business Review Campaigns and More!
            </li>
          </ul>
          <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">GET STARTED
            <span>&#8594;</span></button>
        </div>
      </div>
    </div>
    <div class="moveDown text-center">
      <h1>Let Us Grow Your Business</h1>
      <!--Feature img-->
      <div class="row grow">
        <div class="col-3"><img src="img/in-app-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline">Deliver promotional messages directly to your
              loyal app
              customers</p>
          </div>
        </div>

        <div class="col-3"><img src="img/loyalty2-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline">Offer your customers with in-app mobile coupons </p>
          </div>
        </div>
        <div class="col-3"><img src="img/location-based-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline">Send relevant messages for a pre-defined location or geographic area.</p>
          </div>
        </div>
        <div class="col-3"><img src="img/pyze-icon.png" alt="">
          <div class="row">
            <p class="col-12 subline">Detailed analytics and reporting</p>
          </div>
        </div>
      </div>
      <button onclick="orderLink()" class="btn btn-secondary blurple-bg center moveDown">BUILD YOUR APP NOW
        &#8594;</button>
    </div>

    <div class="container-fluid erie-review blurple-bg moveDown">
      <div class="container row text-center erie-text">
        <div class="col-7 moveDown">
          <i style="font-size: 50px;" class="fas fa-quote-left text-center"></i>
          <h3>Customers use our app to order their replacement parts, accurately, promptly, and most
            importantly, in real time!!! The app Works!</h3><br>
          <p class="white">BILLY KEAN FROM ERIE VEHICLE, EST 1917</p><br><br>
          <img src="img/erie-logo.png" />
        </div>
      </div>

    </div>
  </div>
  <?php include_once('includes/footer.php'); ?>
  <script src="js/vendor/jquery-3.3.1.min.js"></script>
  <script src="js/vendor/popper.min.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
</body>

</html>