<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config/variables.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/lib/pdo_db.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/models/Client.php');

$success = false;
$error_msg = '';

if (!empty($_GET['email']) && !empty($_GET['phone']) && !empty($_GET['id']) && !empty($_GET['time'])){

  // sanitize variables
  $email = filter_var($_GET['email'], FILTER_SANITIZE_EMAIL);
  $phone = filter_var($_GET['phone'], FILTER_SANITIZE_STRING);
  $id    = filter_var($_GET['id'], FILTER_SANITIZE_STRING);
  $time  = filter_var($_GET['time'], FILTER_SANITIZE_STRING);

  // Instantiate Potential
  $client = new Client();

  // Get Potential
  $potential_data = $client->getPotential($email, $phone);
  if (!empty($potential_data)){
    $potential_data[0]->chargebee_id = $id;
    // Copy data to clients table
    if ($client->addClient($potential_data)){
      // Remove client from potential table
      if ($client->removePotential($email, $phone)){
        // If logo exists move logo to logos folder
        if ($potential_data[0]->logo_name != ''){
          if (rename($_SERVER['DOCUMENT_ROOT'].'/incomplete/'.$potential_data[0]->logo_name, $_SERVER['DOCUMENT_ROOT'].'/logos/'.$potential_data[0]->logo_name)){
            $success = true;
          } $error_msg = 'Issue moving logo';
        } 
      } $error_msg = 'Issue removing potential';
    } $error_msg = 'Issue copying client';
  } $error_msg = 'Order does not exist';

  if ($success){
    echo 'Success!';
  } else {
    echo 'Failure!';
    echo '<hr>';
    echo $error_msg;
  }
} else {
  ?>
<h2>Error!</h2>
<?php
}

?>