<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mobilesoft.com</title>
  <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="styles/vendor/slick.css" />
  <link rel="stylesheet" type="text/css" href="styles/vendor/slick-theme.css" />
  <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
  <link rel="stylesheet" href="styles/fonts.css">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="styles/styles.css" />
  <link rel="stylesheet" type="text/css" href="styles/vendor/plyr.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="styles/industry.css" />
</head>

<body>
  <?php include_once('includes/header.php'); ?>

  <div class="container-fluid">
    <!--Split left right-->
    <div class="row bail-upper">
      <div class="col-4"></div>
      <div class="col-4 text-center">
        <img class="img-fluid pt-lg-5 pt-3 pt-md-4 pb-lg-5 pb-3 pb-md-4 mb-lg-3" src="img\top-banner-iphone-legal.png"
          alt="">
      </div>
      <div class="col-4 my-auto">
        <h1 class="text-left">Get your<br>Mobile app<br>for your Firm</h1>
      </div>
    </div>
  </div>

  <div class="d-flex align-items-center justify-content-between px-4">
    <div class="px-1 px-md-3">PROUD PARTNERS OF:</div>
    <div class="px-1 px-md-3"><img class="img-fluid" src="img/partners-pbus.png" alt=""></div>
    <div class="px-1 px-md-3"><img class="img-fluid" src="img/partners-aea.png" alt=""></div>
    <div class="px-1 px-md-3"><img class="img-fluid" src="img/partners-fca.png" alt=""></div>
  </div>


  <div class="row blurple-bg">
    <div class="d-flex mb-0 mt-2 py-0 app-benefits">
      <img class="img-fluid align-self-end" src="/img/Iphone-legal.png" alt="">
      <div class="align-self-start benefit pl-5">
        <h2 class="light-blue bold">Legal Service Apps</h2>
        <h4 class="white">Legal Sercie Apps Your Firm, Your app</h4>
        <p>We build custom Android and iPhone mobile apps that Engage. Interact. Retain. and allow you to stay in touch with your clients and automate your 
        business processes. A mobile app for your Law Firm is an accentual tool to stay competitive in today's market. Get started with us today by clicking the link below
        </p>
        <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">START YOUR BUILD NOW</button>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="text-center pt-3 video-bg m-0 p-0">
      <h2 class="text-white">Legal Service Video</h2>
     
      <div class="video-frame">
        <video poster="/img/bail-video-cover.jpg" id="player" playsinline controls>
          <source src="/video/mobilesoft-bail-app-video.mp4" type="video/mp4" />
        </video>
      </div>
      <div class="play-video-text">
        <img class="img-fluid" src="/img/play-video-label.png" alt="">
      </div>
    </div>
  </div>




  <div class="md-blue-grad">
    <div class="bail-selector my-5">
      <div class="text-center mb-0">
        <h3>APP FEATURES</h3>
      </div>
      <div class="d-flex">
        <div class="text-center e-sign pr-2">
          <span id="e-sign-bail" onclick="industrySelector()">
            <img class="img-fluid w-50" src="img/button-e-sign.png" alt="" id="e-sign-attorney"
              onclick="industrySelector()">
            <h2 class="light-blue">E-Sign</h2>
            <div class="light-grey">Leave the phone lines open for new business by having your clients e-sign
              documents directly from your mobile app. Streamline paperwork and process cases easier.
            </div>
          </span>
        </div>
        <div class="text-center phone">
          <img id="frame" class="img-fluid" src="img/feature-e-sign-bail.png" alt="">
        </div>
        <div class="text-center mobile-payments pl-2">
          <span id="mobile-payments-bail" onclick="industrySelector()">
            <img class="img-fluid w-50" src="img/button-mobile-payments.png" alt="" id="mobile-payments-attorney"
              onclick="industrySelector()">
            <h2 class="light-blue">Mobile Payments</h2>
            <div class="light-grey">Easily invoice and receive payment directly through your app. Auth.net and Paypal
              approved!
            </div>
          </span>
        </div>
      </div>
    </div>
    <div class="special-features text-center">
      <h1 class="col light-blue mb-4">Additional Special Features</h1>
      <div class="row">
        <div class="col white-box text-center">
          <img src="img/icon-call.png" alt="">
          <p class="bold">Call</p>
          <p class="light-grey">
            Allow customers to contact you in one touch.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-directions.png" alt="">
          <p class="bold">Directions</p>
          <p class="light-grey">
            GPS directions allows the customer to locate your firm and plan the quickest route to your firm.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-bail-out.png" alt="">
          <p class="bold">Bail Out</p>
          <p class="light-grey">
            Your clients can use your app to instantly notify you of their arrest, location, and delivery.
          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-accicent-cam.png" alt="">
          <p class="bold">Accident Cam&trade;</p>
          <p class="light-grey">
            Send critical incident information, pictures, and video all in one simple from their mobile phone.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col white-box text-center">
          <img src="img/icon-accicent-cam.png" alt="">
          <p class="bold">Ask the Attorney</p>
          <p class="light-grey">
Clients can easily send you voice recorded questions that your firm can review and reply to.          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-app-schedule.png" alt="">
          <p class="bold">In App Scheduling</p>
          <p class="light-grey">
Open up a new way to accept business by allowing your customers to set appointments through your app          </p>
        </div>
        <div class="col white-box text-center">
          <img src="img/icon-ask-attorney.png" alt="">
          <p class="bold">Leave a Review</p>
          <p class="light-grey">
Integrated directory of all your bail agents and easily acess for clients to get important questions answered          </p>
        </div>
        
        <div class="col white-box text-center">
          <img src="img/icon-free-consultation.png" alt="">
          <p class="bold">Free Consultation</p>
          <p class="light-grey">
Allow your clients to submit and address their situation via mobile app          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="d-flex banner-bail testimonial justify-content-between my-0 py-0">
    <div class="align-self-end app-preview"><img class="img-fluid px-lg-3 px-md-3 px-1"
        src="img/testimonial-phones-law.png"></div>
    <div class="text-center quote align-self-center py-3">
      <div>
        <span class="outer-line align-middle"></span>
        <i style="font-size: 50px;" class="fas fa-quote-left light-blue text-center align-middle px-2"></i>
        <span class="outer-line align-middle"></span>
      </div>
      <div class="white pt-3">I pulled in a $10,000 retainer from a millenial, mobile apps is where the new age is heading and I am jumping ship.
      </div>
      <div class="light-blue bold pt-4">ADAM L. POLLACK</div>
      <div class="white">Criminal Defense Representation in Orlando, Florida</div>
      <div class="white bold">A-Abailable Bail Bonds</div>
    </div>
    <div class="align-self-end avatar"><img class="img-fluid pl-lg-2" src="img/testimonial-person-legal.png">
    </div>
  </div>


  <div class="white-bg my-5 py-3">
    <div class="text-center mb-4">
      <h4 class="light-blue">Some of our Loyal Customers</h4>
    </div>
    <div class="d-flex justify-content-center">
      <div class="customer p-4 border">Client 1</div>
      <div class="customer p-4 border border-left-0">Client 2</div>
      <div class="customer p-4 border border-left-0">Client 3</div>
      <div class="customer p-4 border border-left-0">Client 4</div>
      <div class="customer p-4 border border-left-0">Client 5</div>
      <div class="customer p-4 border border-left-0">Client 6</div>
    </div>
  </div>

  <?php include_once('includes/footer.php'); ?>
  <script src="js/vendor/jquery-3.3.1.min.js"></script>
  <script src="js/vendor/popper.min.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/vendor/plyr.js"></script>
  <script>
  const player = new Plyr('#player');
  </script>
</body>

</html>