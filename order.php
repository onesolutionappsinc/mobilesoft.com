<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
    <link rel="stylesheet" href="styles/vendor/flickity.min.css">
    <link rel="stylesheet" href="styles/fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/order.css">
    <link rel="stylesheet" href="styles/fs-modal.css">
    <link rel="stylesheet" href="styles/vendor/flickity.fullscreen.css">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <title>Mobilesoft</title>
</head>

<body>
    <!-- HEADER -->
    <header>
        <?php include_once('includes/header.php'); ?>
    </header>
    <div class="page mt-2">
        <!-- FORM -->
        <main>
            <div id="form-section">
                <div class="container d-flex flex-row">
                    <form id="order-form" class="main-form d-flex flex-grow-1 mr-md-2">
                        <div class="d-flex flex-column flex-grow-1">

                            <!-- BEGIN WELCOME CONTENT -->
                            <div class="pages page-0 h-100">
                                <div class="d-flex flex-column h-100">
                                    <div class="d-flex flex-column flex-grow-1">
                                        <!-- header -->
                                        <div class="form-header d-flex pt-0">
                                            <div class="step-number px-2">
                                                <img src="img/sm-icon.png" class="pb-2" alt="">
                                            </div>
                                            <div class="header-text pt-2">
                                                <h2>Introduction to Mobilesoft</h2>
                                                <h3>Get Your New Mobile App in 3 EASY STEPS!</h3>
                                            </div>
                                        </div>
                                        <!-- body -->

                                        <div class="flex-grow p-3">
                                            <div class="form-body">
                                                <h3 class="step-heading">Start a New Order</h3>
                                                <div class="divider mb-4"></div>

                                                <div class="form-group row">
                                                    <label for="name"
                                                        class="col-sm-2 col-form-label d-none d-sm-block">Name</label>
                                                    <div class="input-group col-6 col-sm-5">
                                                        <div class="input-group-prepend d-none d-sm-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fa fa-user"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control" id="first-name"
                                                            name="first-name" placeholder="First Name"
                                                            onblur="this.placeholder = 'First Name'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-0"
                                                            required>
                                                    </div>
                                                    <div class="input-group col-6 col-sm-5 pt-sm-0 pl-0">
                                                        <div class="input-group-prepend d-none d-sm-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fa fa-user"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control" id="last-name"
                                                            name="last-name" placeholder="Last Name"
                                                            onblur="this.placeholder = 'Last Name'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-0"
                                                            required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="email"
                                                        class="col-sm-2 col-form-label d-none d-sm-block">Email</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-none d-sm-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-envelope"></i></span>
                                                        </div>
                                                        <input type="email" data-parsley-type="email"
                                                            class="form-control" id="email" name="email"
                                                            placeholder="Email" onblur="this.placeholder = 'Email'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-0"
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone"
                                                        class="col-sm-2 col-form-label d-none d-sm-block">Phone</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-none d-sm-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-phone"></i></i></span>
                                                        </div>
                                                        <input type="tel" class="form-control" id="phone" name="phone"
                                                            placeholder="Phone" onblur="this.placeholder = 'Phone'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-0"
                                                            data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div
                                                        class="d-flex justify-content-md-end justify-content-center pb-3">
                                                        <button type="button" id="step-0-button"
                                                            class="btn btn-continue begin-btn">Begin New
                                                            Order</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-footer p-3 align-text-bottom mt-auto" style="color: black;">
                                            <h3 class="step-heading">Continue an Order</h3>

                                            <div class="form-group row pb-0 mb-0">
                                                <label for="email"
                                                    class="col-sm-2 col-form-label d-none d-sm-block">Email</label>
                                                <div class="input-group col col-sm-10">
                                                    <div class="input-group-prepend d-none d-sm-flex">
                                                        <span class="input-group-text"><i
                                                                class="fas fa-envelope"></i></span>
                                                    </div>
                                                    <input type="email" data-parsley-type="email" class="form-control"
                                                        id="resume-email" name="resume-email" placeholder="Email"
                                                        onblur="this.placeholder = 'Email'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="resume"
                                                        data-parsley-errors-container="#resume-error" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary resume-btn"
                                                            type="button">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row pt-0 mt-0">
                                                <div class="input-group col col-sm-10 offset-sm-2 ">
                                                    <div id="resume-error"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WELCOME CONTENT -->

                            <!-- BEGIN STEP 1 CONTENT -->
                            <div class="pages page-1 h-100 d-none">
                                <div class="d-flex flex-column h-100">
                                    <div class="d-flex flex-column flex-grow-1">
                                        <!-- header -->
                                        <div class="form-header d-flex pt-0">
                                            <div class="step-number px-2">01</div>
                                            <div class="header-text pt-2">
                                                <h2>Thank you for signing up with Mobilesoft</h2>
                                                <h3>we will now begin building out your mobile app</h3>
                                            </div>
                                        </div>
                                        <!-- body -->
                                        <div class="flex-grow p-3 order-body mt-2">
                                            <h4>Please choose from one of the following templates</h4>
                                            <p>The templates listed are completely customizable, the color palate will
                                                change once you receive
                                                a call from one of our team members.</p>

                                            <div class="info-container">
                                                <i class="flickity-fs-button fas fa-times-circle fa-2x d-none"
                                                    style="z-index: 3;" data-dismiss="modal"></i>
                                                <div class="main-carousel">

                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-1.jpg');"
                                                        title="template-1"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-2.jpg');"
                                                        title="template-2"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-3.jpg');"
                                                        title="template-3"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-4.jpg');"
                                                        title="template-4"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-5.jpg');"
                                                        title="template-5"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-1.jpg');"
                                                        title="template-1"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-2.jpg');"
                                                        title="template-2"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-3.jpg');"
                                                        title="template-3"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-4.jpg');"
                                                        title="template-4"></div>
                                                    <div class="carousel-cell"
                                                        style="background-image: url('img/full/template-5.jpg');"
                                                        title="template-5"></div>

                                                </div>
                                            </div>

                                        </div>
                                        <input id="template-name" type="hidden" value="template-1" name="template-name">
                                        <input id="template-id" type="hidden" value="0" name="template-id">
                                        <div class="divider"></div>
                                        <div class="flex-grow p-3 order-body">
                                            <h4>Now that you chose the look you wanted, please upload your current logo
                                            </h4>
                                            <p>If you don't have a logo, don't worry. One of our designers will assist
                                                you.</p>
                                            <div class="info-container pl-2 pr-1 py-3 m-0">

                                                <!-- Logo upload -->
                                                <label class="btn btn-primary btn-sm" id="upload-logo-button"
                                                    for="upload-logo">
                                                    <input id="upload-logo" name="uploaded-logo" type="file"
                                                        accept="image/*,.ai,.pdf,.psd" style="display:none"
                                                        onchange="uploadLogo()">
                                                    <i class="fas fa-plus"></i> Upload your Logo
                                                </label>
                                                <div id="upload-error" class="alert-danger font-weight-bold d-none">
                                                </div>
                                                <div class="progress d-none">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                                        aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <p class="logo-format"><span class="font-weight-bold">File Format</span>
                                                    - AI / IMAGE / PDF /
                                                    PSD (Max: 8MB)</p>
                                                <div class="row logo-info d-none">
                                                    <div class="col-6">
                                                        <div class="logo-preview text-center">
                                                            <img class="img-fluid pb-3" src=""
                                                                style="max-height:200px;">
                                                        </div>
                                                    </div>
                                                    <div class="col-6 pb-2">
                                                        <p class='label label-info' id="upload-file-name"
                                                            style="overflow:hidden; text-overflow: ellipsis;"></p>
                                                        <p class='label label-info' id="upload-file-size"></p>
                                                        <div class="btn btn-danger btn-sm remove-logo">Remove</div>
                                                    </div>
                                                </div>
                                                <div class="divider logo-url"></div>
                                                <div class="form-group row m-0 p-0 py-2 logo-url">
                                                    <label for="logo-url"
                                                        class="col-sm-3 col-form-label d-none d-sm-block ml-0 pl-1">
                                                        <h4 class="text-nowrap">Logo URL</h4>
                                                    </label>
                                                    <div class="input-group col col-sm-9">
                                                        <input type="url" class="form-control" id="logo_url"
                                                            name="logo-url" placeholder="https://yourcompany.com/logo"
                                                            onblur="this.placeholder = 'https:\/\/yourcompany.com/logo'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                                <div class="divider logo-checkbox"></div>
                                                <div class="form-group row m-0 p-0 pt-2 logo-checkbox">
                                                    <div class="form-check col">
                                                        <input class="form-check-input" type="checkbox" value=""
                                                            id="need-logo">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            <p class="m-0 p-0">Check here if you do not have a logo or
                                                                if you need a new one.</p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="flex-grow p-3 order-body">
                                            <div class="info-container pl-2 pr-1 py-3 m-0">
                                                <div class="form-group row m-0 p-0">
                                                    <label for="business-url"
                                                        class="col-sm-3 col-form-label d-none d-sm-block ml-0 pl-1">
                                                        <h4 class="text-nowrap">Add Website</h4>
                                                    </label>
                                                    <div class="input-group col col-sm-9">
                                                        <input type="url" class="form-control" id="business_website"
                                                            name="business_website"
                                                            placeholder="https://yourcompany.com"
                                                            onblur="this.placeholder = 'https:\/\/yourcompany.com'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1"
                                                            required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="flex-grow p-3 order-body">
                                            <!-- Social Media Links -->
                                            <h4 class="pt-1">Add Social Media Links</h4>
                                            <p>Please select the social media links that apply to you and your business
                                            </p>
                                            <div class="form-group social-media d-flex pb-2 justify-content-between ">
                                                <div><label id="youtube_url-check" class="image-checkbox"><img
                                                            src="img/sm-youtube-color.png" alt=""><input type="checkbox"
                                                            class="sm-chk" name="image[]" data-name="youtube_url-input"
                                                            value="" /></label>
                                                </div>
                                                <div><label id="instagram_url-check" class="image-checkbox"><img
                                                            src="img/sm-instagram-color.png" alt=""><input
                                                            type="checkbox" class="sm-chk" name="image[]"
                                                            data-name="instagram_url-input" value="" /></label>
                                                </div>
                                                <div><label id="yelp_url-check" class="image-checkbox"><img
                                                            src="img/sm-yelp-color.png" alt=""><input type="checkbox"
                                                            class="sm-chk" name="image[]" data-name="yelp_url-input"
                                                            value="" /></label>
                                                </div>
                                                <div><label id="facebook_url-check" class="image-checkbox"><img
                                                            src="img/sm-facebook-color.png" alt=""><input
                                                            type="checkbox" class="sm-chk" name="image[]"
                                                            data-name="facebook_url-input" value="" /></label>
                                                </div>
                                                <div><label id="twitter_url-check" class="image-checkbox"><img
                                                            src="img/sm-twitter-color.png" alt=""><input type="checkbox"
                                                            class="sm-chk" name="image[]" data-name="twitter_url-input"
                                                            value="" /></label>
                                                </div>
                                                <div><label id="linkedin_url-check" class="image-checkbox"><img
                                                            src="img/sm-linkedin-color.png" alt=""><input
                                                            type="checkbox" class="sm-chk" name="image[]"
                                                            data-name="linkedin_url-input" value="" /></label>
                                                </div>
                                            </div>

                                            <div class="pl-sm-3">
                                                <div id="youtube_url-input" class="form-group row d-none">
                                                    <label for="sm-youtube"
                                                        class="col-sm-2 col-form-label d-none d-sm-block sm-label youtube-bg">YouTube</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-sm-none d-flex">
                                                            <span class="input-group-text youtube-bg"><i
                                                                    class="fab fa-youtube"></i></span>
                                                        </div>
                                                        <input type="url" class="form-control" id="youtube_url"
                                                            name="sm-youtube" placeholder="https://youtube.com/..."
                                                            onblur="this.placeholder = 'https:\/\/youtube.com/...'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                                <div id="instagram_url-input" class="form-group row d-none">
                                                    <label for="sm-instagram"
                                                        class="col-sm-2 col-form-label d-none d-sm-block sm-label instagram-bg">Instagram</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-sm-none d-flex">
                                                            <span class="input-group-text instagram-bg"><i
                                                                    class="fab fa-instagram"></i></span>
                                                        </div>
                                                        <input type="url" class="form-control" id="instagram_url"
                                                            name="sm-instagram" placeholder="https://instagram.com/..."
                                                            onblur="this.placeholder = 'https:\/\/instagram.com/...'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                                <div id="yelp_url-input" class="form-group row d-none">
                                                    <label for="sm-yelp"
                                                        class="col-sm-2 col-form-label d-none d-sm-block sm-label yelp-bg">Yelp</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-sm-none d-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fab fa-yelp"></i></span>
                                                        </div>
                                                        <input type="url" class="form-control" id="yelp_url"
                                                            name="sm-yelp" placeholder="https://www.yelp.com/biz/..."
                                                            onblur="this.placeholder = 'https:\/\/www.yelp.com/biz/...'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                                <div id="facebook_url-input" class="form-group row d-none">
                                                    <label for="sm-facebook"
                                                        class="col-sm-2 col-form-label d-none d-sm-block sm-label facebook-bg">Facebook</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-sm-none d-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fab fa-facebook-square"></i></span>
                                                        </div>
                                                        <input type="url" class="form-control" id="facebook_url"
                                                            name="sm-facebook"
                                                            placeholder="https://www.facebook.com/..."
                                                            onblur="this.placeholder = 'https:\/\/www.facebook.com/...'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                                <div id="twitter_url-input" class="form-group row d-none">
                                                    <label for="sm-twitter"
                                                        class="col-sm-2 col-form-label d-none d-sm-block sm-label twitter-bg">Twitter</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-sm-none d-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fab fa-twitter-square"></i></span>
                                                        </div>
                                                        <input type="url" class="form-control" id="twitter_url"
                                                            name="sm-twitter" placeholder="https://twitter.com/..."
                                                            onblur="this.placeholder = 'https:\/\/twitter.com/...'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                                <div id="linkedin_url-input" class="form-group row d-none">
                                                    <label for="sm-linkedin"
                                                        class="col-sm-2 col-form-label d-none d-sm-block sm-label linkedin-bg">LinkedIn</label>
                                                    <div class="input-group col col-sm-10">
                                                        <div class="input-group-prepend d-sm-none d-flex">
                                                            <span class="input-group-text"><i
                                                                    class="fab fa-linkedin"></i></span>
                                                        </div>
                                                        <input type="url" class="form-control" id="linkedin_url"
                                                            name="sm-linkedin"
                                                            placeholder="https://www.linkedin.com/company/..."
                                                            onblur="this.placeholder = 'https:\/\/www.linkedin.com/company/...'"
                                                            onfocus="this.placeholder = ''" data-parsley-group="page-1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- footer -->
                                        <div class="form-footer p-3 align-text-bottom mt-auto">
                                            <div class="d-flex justify-content-md-end justify-content-center">
                                                <button type="button"
                                                    class="btn btn-continue step-2-btn">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END STEP 1 CONTENT -->

                            <!-- BEGIN STEP 2 CONTENT -->
                            <div class="pages page-2 h-100 d-none">
                                <div class="d-flex flex-column h-100">
                                    <div class="d-flex flex-column flex-grow-1">
                                        <!-- header -->
                                        <div class="form-header d-flex pt-0">
                                            <div class="step-number px-2">02</div>
                                            <div class="header-text pt-2">
                                                <h2>Business Information</h2>
                                                <h3>Details About Your Business</h3>
                                            </div>
                                        </div>
                                        <!-- body -->
                                        <div class="flex-grow p-3">
                                            <h3 class="step-heading">Business Details</h3>
                                            <div class="divider mb-4"></div>
                                            <div class="form-group row">
                                                <label for="business-name"
                                                    class="col-sm-2 col-form-label d-none d-sm-block">Company</label>
                                                <div class="input-group col col-sm-10">
                                                    <div class="input-group-prepend d-none d-sm-flex">
                                                        <span class="input-group-text"><i
                                                                class="fas fa-building"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="business_name"
                                                        name="business-name" placeholder="Business Name"
                                                        onblur="this.placeholder = 'Business Name'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="page-2"
                                                        required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="business-address"
                                                    class="col-sm-2 col-form-label d-none d-sm-block">Address</label>
                                                <div class="input-group col col-sm-10">
                                                    <div class="input-group-prepend d-none d-sm-flex">
                                                        <span class="input-group-text"><i
                                                                class="fas fa-address-card"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="business_address"
                                                        name="business-address" placeholder="Address"
                                                        onblur="this.placeholder = 'Address'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="page-2"
                                                        required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="name"
                                                    class="col-sm-2 col-form-label d-none d-sm-block"></label>
                                                <div class="input-group col-4 col-sm-4 pr-1 mr-0">
                                                    <div class="input-group-prepend d-none d-sm-flex">
                                                        <span class="input-group-text"><i
                                                                class="far fa-address-card"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="business_city"
                                                        name="business-city" placeholder="City"
                                                        onblur="this.placeholder = 'City'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="page-2"
                                                        required>
                                                </div>
                                                <div class="input-group col-4 col-sm-3 pt-sm-0 pl-0 mr-0 pr-0">
                                                    <input type="text" class="form-control" id="business_state"
                                                        name="business-state" placeholder="State"
                                                        onblur="this.placeholder = 'State'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="page-2"
                                                        required>
                                                </div>
                                                <div class="input-group col-4 col-sm-3 pt-sm-0 pl-1 ml-0">
                                                    <input type="text" class="form-control" id="business_zip"
                                                        name="business-zip" placeholder="Zip"
                                                        onblur="this.placeholder = 'Zip'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="page-2"
                                                        required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="business-phone"
                                                    class="col-sm-2 col-form-label d-none d-sm-block">Phone</label>
                                                <div class="input-group col col-sm-10">
                                                    <div class="input-group-prepend d-none d-sm-flex">
                                                        <span class="input-group-text"><i
                                                                class="fas fa-phone"></i></span>
                                                    </div>
                                                    <input type="tel" class="form-control" id="business_phone"
                                                        name="business-phone" placeholder="Business Phone"
                                                        onblur="this.placeholder = 'Business Phone'"
                                                        onfocus="this.placeholder = ''" data-parsley-group="page-2"
                                                        data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" required>
                                                </div>
                                            </div>
                                            <!-- footer -->
                                        </div>
                                        <div class="form-footer p-3 align-text-bottom mt-auto">
                                            <div class="d-flex justify-content-md-end justify-content-center">
                                                <button type="button"
                                                    class="btn btn-continue step-1-btn mr-2">Back</button>
                                                <button type="button"
                                                    class="btn btn-continue step-3-btn">Continue</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END STEP 2 CONTENT -->

                            <!-- BEGIN STEP 3 CONTENT -->
                            <div class="pages page-3 h-100 d-none">
                                <div class="d-flex flex-column h-100">
                                    <div class="d-flex flex-column flex-grow-1">
                                        <!-- header -->
                                        <div class="form-header d-flex pt-0">
                                            <div class="step-number px-2">03</div>
                                            <div class="header-text pt-2">
                                                <h2>Finalize your Order</h2>
                                                <h3>Verify details and submit payment</h3>
                                            </div>
                                        </div>
                                        <!-- body -->
                                        <div class="flex-grow p-3 summary">
                                            <h3 class="step-heading">Order Summary</h3>
                                            <div class="divider mb-4"></div>
                                            <div class="row">
                                                <div class="col-7 col-md-8">
                                                    <h4>Address</h4>
                                                    <p class="m-0 p-0 summary-company">Company</p>
                                                    <p class="m-0 p-0 summary-address">Address</p>
                                                    <p class="m-0 p-0 summary-address-2">Address 2</p>
                                                    <div class="divider my-4"></div>
                                                    <h4>Phone</h4>
                                                    <p class="m-0 p-0 summary-phone">Phone</p>
                                                    <div class="divider my-3"></div>
                                                    <h4>Email</h4>
                                                    <p class="m-0 p-0 summary-email">Email</p>
                                                    <div class="divider my-3"></div>
                                                    <h4>Website</h4>
                                                    <p class="m-0 p-0 summary-website">Website</p>
                                                    <div class="divider my-3"></div>
                                                    <h4>Social Media Links</h4>
                                                    <div class="d-flex pt-3">
                                                        <div class="social-1 summary-social px-1"></div>
                                                        <div class="social-2 summary-social px-1"></div>
                                                        <div class="social-3 summary-social px-1"></div>
                                                        <div class="social-4 summary-social px-1"></div>
                                                        <div class="social-5 summary-social px-1"></div>
                                                        <div class="social-6 summary-social px-1"></div>
                                                    </div>
                                                </div>
                                                <div class="col-5 col-md-4 summary-template">
                                                    <img class="img-fluid" src="" alt="">
                                                </div>
                                            </div>
                                            <h4 class="my-3">App Summary & Special Comments</h4>
                                            <div class="divider mb-2"></div>
                                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Itaque qui
                                                officiis similique
                                                explicabo. Sint neque mollitia perspiciatis aut, sunt omnis, dolorum
                                                aliquid tempora totam
                                                dolore, quasi obcaecati vel? Tempore, sit!</p>
                                        </div>
                                        <!-- footer -->
                                        <div class="form-footer p-3 align-text-bottom mt-auto">
                                            <div class="d-flex justify-content-md-end justify-content-center">
                                                <button type="button"
                                                    class="btn btn-continue step-2-btn mr-2">Back</button>
                                                <a class="btn payment-btn" href="javascript:void(0)"
                                                    data-cb-type="checkout" data-cb-plan-id="mobilesoft-test">Proceed to
                                                    Payment</a>
                                                <!-- <button type="button" class="btn btn-continue step-3-btn">Finish</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END STEP 3 CONTENT -->

                        </div>
                    </form>

                    <!-- SIDEBAR -->

                    <!-- <div class="sidebar d-none d-md-block p-0 align-items-end"> 100% height sidebar-->
                    <div class="sidebar d-none d-md-block p-0 align-self-baseline">
                        <div class="d-flex flex-column pt-3 px-2 h-100">
                            <div class="sidebar-step step-1 inactive-step d-flex p-0 m-0">
                                <div class="step-number pr-2 pl-3 pt-1">01</div>
                                <div class="step-text pt-2">
                                    <h2>Design Your App</h2>
                                    <h3>Choose the Look and Feel</h3>
                                </div>
                            </div>
                            <div class="sidebar-step step-2 inactive-step d-flex pt-0">
                                <div class="step-number pr-2 pl-3 pt-1">02</div>
                                <div class="step-text pt-2">
                                    <h2>About Your Business</h2>
                                    <h3>Basic Information About Your Business</h3>
                                </div>
                            </div>
                            <div class="sidebar-step step-3 inactive-step d-flex pt-0 mb-5">
                                <div class="step-number pr-2 pl-3 pt-1">03</div>
                                <div class="step-text pt-2">
                                    <h2>Verfication and Payment</h2>
                                    <h3>Check the Details and Proceed</h3>
                                </div>
                            </div>
                            <div class="helpful-tips p-3 pb-5 mx-2 my-3 align-text-bottom mt-auto">
                                <div class="d-flex pb-2">
                                    <div class="icon"><img src="img/light-bulb.png" alt=""></div>
                                    <div class="title align-self-center pl-2">
                                        <h2>Helpful Tips</h2>
                                    </div>
                                </div>
                                <!-- TIP CONTENT -->
                                <p class="tips tip-0 pb-5">Your company can send special offers to customers who are in
                                    close
                                    proximity to your store or office with the help of Geofencing.</p>
                                <p class="tips tip-1 pb-5 d-none">Mobile apps can be successfully used to attract
                                    attention to your
                                    brand
                                    through various marketing campaigns.</p>
                                <p class="tips tip-2 pb-5 d-none">Customers can book tables, order food or pay for their
                                    order using
                                    your
                                    app on their smartphone.</p>
                                <p class="tips tip-3 pb-5 d-none">Integrate loyalty programs into your mobile app and
                                    share useful
                                    promotions, discounts, or bonuses with customers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STEP INDICATOR -->
            <div id="step-indicator">
                <div class="container d-flex flex-row pt-3">
                    <div class="main-form flex-grow-1 mr-md-2 d-flex justify-content-around">
                        <div id="box-0" class="step-box-welcome step-box-0 step-complete h-50">&nbsp;</div>
                        <div id="box-1" class="step-box step-box-1 step-incomplete h-50">&nbsp;</div>
                        <div id="box-2" class="step-box step-box-2 step-incomplete h-50">&nbsp;</div>
                        <div id="box-3" class="step-box step-box-3 step-incomplete h-50">&nbsp;</div>
                    </div>
                    <div class="sidebar d-none d-md-block"></div>
                </div>
            </div>

            <!-- HELP BANNER -->
            <div id="help-banner">
                <div class="container pt-4 pb-5">
                    <div class="help-container d-flex">
                        <div class="help-image pl-1">
                            <img src="img/help-avatar.png" alt="">
                        </div>
                        <div class="help-content pt-2 pt-md-3 pl-2 pl-md-0">
                            <div class="d-none d-md-block">
                                <!-- STANDARD HELP MESSAGE -->
                                <h2>If you need help with filling out the form, contact our Help Desk</h2>
                                <h3 class="pt-1">Ask for Jameson at 123-456-7890 or Email help@mobilesoft.com</h3>
                            </div>
                            <div class="d-md-none">
                                <!-- MOBILE HELP MESSAGE -->
                                <h2>Contact our Help Desk if you need assistance</h2>
                                <h3 class="pt-1">Tel: 123-456-7890 | help@mobilesoft.com</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- LOWER CONTENT -->
        <div id="lower-section">
            <!-- FOOTER -->
            <?php include_once('includes/footer.php'); ?>
        </div>
    </div>

    <!-- RESUME NOT FOUND MODAL -->
    <div class="modal fade text-dark" id="notFoundModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Incomplete Order Not Found</h5>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="js/vendor/jquery-3.3.1.min.js"></script>
    <script src="js/vendor/jquery.mask.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/flickity.pkgd.js"></script>
    <script src="js/vendor/flickity.fullscreen.js"></script>
    <script src="js/vendor/parsley.min.js"></script>
    <script src="js/order-form.js"></script>
    <script src="js/main.js"></script>
    <script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="mobilesoft-test"></script>
</body>

</html>