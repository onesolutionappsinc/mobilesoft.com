<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mobilesoft.com</title>
    <link rel="stylesheet" href="/styles/vendor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/styles/vendor/slick.css" />
    <link rel="stylesheet" type="text/css" href="/styles/vendor/slick-theme.css" />
    <link rel="stylesheet" href="/styles/vendor/fontawesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="/styles/styles.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/styles/mobile.css" />
</head>

<body>
    <?php include_once('../includes/header.php'); ?>
    <div class="container-fluid">
        <div class="col white light-blue-bg">
            <div class="row">
                <ul class="grey get-an-app center col">
                    <h1 class="text-center">It's time you get a mobile app</h1>
                    <hr class="white-op">
                    <div class="row">
                        <li class="col-3">
                            <img class="" src="/img/icon-1.png" alt="">

                        </li>
                        <div class="text-app col-9">
                            <h4 class="white">75% of all digital traffic is branded through mobile</h4>
                            <li class="subline">It's time to Connect your client to your brand</li>
                        </div>
                    </div>

                    <hr class="white-op">
                    <div class="row">
                        <li class="col-3">
                            <img src="/img/icon-2.png" alt="">
                        </li>
                        <div class="text-app col-9">
                            <h4 class="white">Deliver custom promotions, announcements, and loyalty.</h4>
                            <li class="subline">Connect with new audiences and different demographics</li>
                        </div>
                    </div>

                    <hr class="white-op">
                    <div class="row">
                        <li class="col-3">
                            <img src="/img/icon-3.png" alt="">
                        </li>
                        <div class="text-app col-9">
                            <h4 class="white">Simple to use marketing platform</h4>
                            <li class="subline">Add features to your account through our back end</li>
                        </div>
                    </div>
                </ul>
            </div>
            <div class="text-center">
                <button onclick="orderLink()" class="btn btn-secondary blurple-bg">SIGN UP NOW</button>
            </div>
            <div class="row">
                <img class="m-auto py-5" src="/img/phone-frame.png" alt="">
            </div>
        </div>

        <div class="white-bg row">
            <div class="white-box white-box-index col">
                <h4 class="light-blue slimmer text-center">The Ultimate Print Marketing Materials</h4>
                <p class="light-grey text-center"> Get this amazing bundle of print marketing materials to help showcase
                    your
                    business.
                </p>
                <ul class="grey">
                    <li><i class="fas fa-check"></i>Increase visibility and downloads</li>
                    <li><i class="fas fa-check"></i>20% visability after 90 days accross all industries</li>
                    <li><i class="fas fa-check"></i>Additional Google Rankings</li>
                    <li><i class="fas fa-check"></i>10+ sessions per month. Per user</li>
                </ul>
                <div class="text-center">
                    <button onclick="orderLink()" class="btn btn-secondary blurple-bg">SIGN UP NOW
                        <span>&#8594;</span></button>
                </div>
            </div>
        </div>
        <div class="light-blue-bg phone-person">
            <div class="upgrade">
                <h3 class="white">Upgrade <span>Your Marketing</span> Package</h3>
                <img style="visibility: hidden; margin: -55% 0 0;" class="" src="/img/dude.png" />

            </div>
        </div>
        <div id="order-slider ">
            <div class="row center">
                <h3 class="light-blue text-center col-12">Custom App Design Made Easy For Your Business</h3>
                <p class="light-grey text-center col-12">With World-Class marketing support</p>
                <div class="center order-slider">
                    <img class="screen " src="/img/Screen-1.png" />
                </div>
            </div>
        </div>
        <div class="row build-btn text-center">
            <p class="col-12">Update your app in real-time with new content and products</p>
            <button style="
    width: 57%;
" onclick="orderLink()" class="btn btn-secondary blurple-bg center">START YOUR BUILD NOW
                <span>&#8594;</span></button>
        </div>
        <div id="four" class="text-center center ">
            <h1 class="light-blue">Features Built With The Mobile App</h1>
            <p class="subline">Update your app in real-time with new content and products</p>
            <div class="column">
                <img id="frame" src="/img/push-notifications.png" alt="" style="width: 55%;">
            </div>
            <div class="squares-container row">
                <div class="square col-3">
                    <div class="square-content text-center " id="push-notification" onclick="changePhone()">
                        <img id="push-notification" src="/img/push-icon.png" />
                        <p id="push-notification" class="light-blue">Push Notifications</p>
                        <p id="push-notification" class="clicker-subline grey" style="word-wrap:break-word;">update your
                            app in
                            real-time
                            with
                            new
                            content and products</p>
                    </div>
                </div>
                <div class="square col-3">
                    <div class="square-content text-center" id="geofence" onclick="changePhone()">
                        <img id="geofence" src="/img/geofence-icon.png" />
                        <p id="geofence" class="light-blue">Geo Fencing</p>
                        <p id="geofence" class="clicker-subline grey" style="word-wrap:break-word;">update your app in
                            real-time
                            with
                            new
                            content and products</p>
                    </div>
                </div>
                <div class="square col-3">
                    <div class="square-content text-center" id="loyalty" onclick="changePhone()">
                        <img id="loyalty" src="/img/loyalty-icon.png" />
                        <p id="loyalty" class="light-blue">Loyalty</p>
                        <p id="loyalty" class="clicker-subline grey" style="word-wrap:break-word;">update your app in
                            real-time
                            with
                            new
                            content and products</p>
                    </div>
                </div>
                <div class="square col-3">
                    <div class="square-content text-center" id="intake" onclick="changePhone()">
                        <img id="intake" src="/img/intake-icon.png" />
                        <p id="intake" class="light-blue">Intake Forms</p>
                        <p id="intake" class="clicker-subline grey" style="word-wrap:break-word;">update your app in
                            real-time
                            with
                            new
                            content and products</p>
                    </div>
                </div>
            </div>
            <div id="testimonial" class="row">
                <!-- Testimonial slider-->
                <div class="blue-box text-center center slider company-review">
                    <!--Company/ name-->

                    <div>
                        <p class="white bold">Chris Lucario</p>
                        <p class="white-op">A-Abailable Bail Bonds</p>
                        <p class="white"> By integrating this app into my business it has given me ease of mind when my
                            clients attend court by giving them the ability to take photos of their continuance order,
                            receipts for payments or even disposition papers and allowing them to submit it directly
                            through the app.</p>
                    </div>
                    <div>
                        <p class="white bold">Adam Pollock</p>
                        <p class="white-op">Adam Pollock and Associates</p>
                        <p class="white"> I pulled in a $10,000 retainer from a millennial, mobile apps is where the
                            new age is heading and I am jumping ship. </p>
                    </div>
                    <div>
                        <p class="white bold">Jeremy Rowland</p>
                        <p class="white-op">Triple R Furniture/Pawn Shop</p>
                        <p class="white"> Plan it, push it out, Get results! Team up with Mobilesoft now.</p>
                    </div>
                    <div>
                        <p class="white bold">William Keen</p>
                        <p class="white-op">Erie Vehicle Company</p>
                        <p class="white"> Customers, needing replacements truck parts quickly, use our "MobileSoft" App
                            to "order" there replacement parts, accurately, promptly and most importantly, in real
                            time.!! The app "Works"</p>
                    </div>
                    <div>
                        <p class="white bold">Sam Marvin</p>
                        <p class="white-op">Echo & Rig</p>
                        <p class="white"> Review, order and pick up, It cant get easier then that. Mobilesoft
                            understood exactly what we wanted to execute and made it happen. </p>
                    </div>
                    <div>
                        <p class="white bold">Shahab Zargari</p>
                        <p class="white-op">AMA Las Vegas</p>
                        <p class="white"> As the president of the American marketing Association Las Vegas chapter I
                            was looking for ways to stay connected with our members. We never thought an app would be
                            useful, let alone integral. Boy were we wrong. </p>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once('../includes/footer.php'); ?>
        <script src="/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="/js/vendor/popper.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/main.js"></script>
        <script type="text/javascript" src="/js/vendor/slick.min.js"></script>
        <script type="text/javascript">
        $('.slider').slick({
            arrows: false,
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,

            autoplay: true,
            autoplaySpeed: 2000,

        });
        </script>
</body>

</html>