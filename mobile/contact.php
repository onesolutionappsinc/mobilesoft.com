<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>contact Mobile</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/styles/vendor/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/styles/vendor/slick.css" />
  <link rel="stylesheet" type="text/css" href="/styles/vendor/slick-theme.css" />
  <link rel="stylesheet" href="/styles/vendor/fontawesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="/styles/styles.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="/styles/mobile.css" />
</head>

<body>
  <?php include_once('../includes/header.php'); ?>
  <div class="container-fluid">
    <hr class="mt-0">
    <h3 class="text-center light-blue">Contact Us </h3>
    <hr>

    <div class="row text-center">
      <div class="col">
        <div class="contact-header">
          <div>
            <img src="/img/m-phone-icon.png">
            <a href="tel:8887148014">Toll Free: 888.714.8014</a>
          </div>
          <div>
            <img src="/img/m-mobile-phone-icon.png">
            <a href="tel:7028058500">Support: 702.805.8500</a>
          </div>
          <div>
            <img src="/img/m-email-icon.png">
            <a href="mailto:support@mobilesoft.com">Email: support@mobilesoft.com</a>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="container">
        <div class="contact-box col grey">
          <form id="contact-us-form">
            <div class="form-group row">
              <label for="first-name" class="col-sm-4 col-form-label">First Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="first-name" name="first-name" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="company-name" class="col-sm-4 col-form-label">Company Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="company-name" name="company-name" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="phone-number" class="col-sm-4 col-form-label">Phone Number</label>
              <div class="col-sm-8">
                <input type="tel" class="form-control" id="phone-number" name="phone-number"
                  data-parsley-trigger="change" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="email" class="col-sm-4 col-form-label">Email</label>
              <div class="col-sm-8">
                <input type="email" class="form-control" id="email" name="email" data-parsley-trigger="change" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="existing-client-group" class="col-sm-4 col-form-label">Are you an existing client</label>
              <div class="col-sm-8" id="existing-client-group">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="existing" id="existing-client"
                    value="existing-client" required>
                  <label class="form-check-label" for="existing-client">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="existing" id="nonexisting-client"
                    value="nonexisting-client" checked>
                  <label class="form-check-label" for="nonexisting-client">No</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="message" class="col-sm-4 col-form-label">Message</label>
              <div class="col-sm-8">
                <textarea class="form-control" id="message" name="message" rows="3" data-parsley-trigger="keyup"
                  data-parsley-minlength="20" data-parsley-maxlength="1000" data-parsley-validation-threshold="10"
                  required></textarea>
              </div>
            </div>
            <div class="form-group row">
              <div class="col col-12 center ">
                <button type="submit" style="width: 224px;" class="btn btn-secondary blurple-bg center right">SUBMIT
                  INFORMATION
                  &#8594;</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php include_once('../includes/footer.php'); ?>
  <script src="/js/vendor/jquery-3.3.1.min.js"></script>
  <script src="/js/vendor/popper.min.js"></script>
  <script src="/js/vendor/bootstrap.min.js"></script>
  <script src="/js/vendor/parsley.min.js"></script>
  <script src="/js/main.js"></script>
</body>

</html>