<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>contact Mobile</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/styles/vendor/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/styles/vendor/slick.css" />
  <link rel="stylesheet" type="text/css" href="/styles/vendor/slick-theme.css" />
  <link rel="stylesheet" href="/styles/vendor/fontawesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="/styles/styles.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="/styles/mobile.css" />
</head>

<body>
  <?php include_once('../includes/header.php'); ?>
  <div class="container-fluid">
    <hr class="mt-0">
    <h2 class="light-blue slimmer text-center">No Contract. Straight Foward</h2>
    <h5 class="grey text-center"> Design. Build. Publish. Your Mobile App</h5>
    <div class="col text-center">
      <p class="grey text-center">on</p>
      <img src="/img/play-button.png" alt="">
      <img src="/img/ios-button.png" alt="">
      <img src="/img/amazon-button.png" alt="">
    </div>

    <br>

    <div class="pricing-box white-box moveDown">
      <div class="text-center">
        <p class="grey">EASY SET UP</p>
        <h3 class="slimmer light-blue" style="font-size: 4.5rem;">$199</h3>
      </div>
      <hr>
      <ul class="grey pricing-col">
        <li><i class="fas fa-check"></i>Complete Custom Design</li>
        <li><i class="fas fa-check"></i>Custom Feature Set</li>
        <li><i class="fas fa-check"></i>Industry Specific Tools</li>
        <li><i class="fas fa-check"></i>Live Customer Support Team</li>
        <li><i class="fas fa-check"></i>Dedicated Mobile Expert Rep</li>
        <li><i class="fas fa-check"></i>Marketing Dashboard</li>
        <li><i class="fas fa-check"></i>Enhanced Analytics<p>Listed on: <br /> Google Play Store <br>
            Apple
            App Store <br /> Website
            Direct Download</p>
        </li>
        <li><i class="fas fa-check"></i>One-Time Easy Set Up Fee</li>
      </ul>
      <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">GET STARTED
        <span>&#8594;</span></button>


    </div>
    <div class="pricing-box white-box moveDown">
      <div class="text-center ">
        <p class="grey">NO CONTRACT SUBSCRIPTION</p>
        <h3 style="font-size: 4.5rem;" class="slimmer light-blue">$79<span class="month grey">/mo</span>
        </h3>
      </div>
      <hr>
      <ul class="grey pricing-col">
        <li><i class="fas fa-check"></i>Unlimited Push Notifications</li>
        <li><i class="fas fa-check"></i>Unlimited Customer Downloads</li>
        <li><i class="fas fa-check"></i>Location-Based (GEO) Messaging</li>
        <li><i class="fas fa-check"></i>Free Android & IOS Upgrades</li>
        <li><i class="fas fa-check"></i>Integrated Maps</li>
        <li><i class="fas fa-check"></i>Camera/Mic Features</li>
        <li><i class="fas fa-check"></i>Social Media</li>
        <li><i class="fas fa-check"></i>Reviews</li>
        <li><i class="fas fa-check"></i>Coupons & Loyalty Program</li>
        <li><i class="fas fa-check"></i>Enhanced Analytics & Reporting</li>
        <li><i class="fas fa-check"></i>Live Customer Support Team</li>
        <li><i class="fas fa-check"></i>Dedicated Mobile Expert Rep</li>
        <li><i class="fas fa-check"></i>In-App Advertising+</li>
        <li><i class="fas fa-check"></i>Mobile Payments* ($5/mo)</li>
        <li><i class="fas fa-check"></i>eSign*($20/mo)</li>
      </ul>
      <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">GET STARTED
        <span>&#8594;</span></button>

    </div>
    <div class="pricing-box white-box moveDown">
      <div class="text-center">
        <p class="grey">YOU WIN. WE WIN. CAMPAIGN</p>
        <h3 style="font-size: 4.5rem;" class="slimmer light-blue">$39<span class="month grey">/mo</span>
        </h3>
        <h3 class="light-blue bold">INTRODUCTORY OFFER</h3>
      </div>
      <hr>
      <ul class="grey pricing-col">
        <li><i class="fas fa-check"></i>Let our dedicated Campaign Managers promote your mobile app!
        </li>

        <li><i class="fas fa-check"></i>Increase Google Rankings
        </li>

        <li><i class="fas fa-check"></i>Promotions & Announcements
        </li>
        <li><i class="fas fa-check"></i>In-App Marketing Messages</li>
        <li><i class="fas fa-check"></i>Print Kit Materials for your location</li>
        <li><i class="fas fa-check"></i>Social Media Promotions</li>
        <li><i class="fas fa-check"></i>Business Review Campaigns and More!
        </li>
      </ul>
      <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">GET STARTED
        <span>&#8594;</span></button>
    </div>
    <div class="moveDown text-center">
      <h2 class="slimmer light-blue">Let Us Grow Your Business</h2>
      <!--Feature img-->
      <div class="row grow">
        <div class="col-6">
          <img src="/img/in-app-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline2">Deliver promotional messages directly to your
              loyal app
              customers</p>
          </div>
        </div>

        <div class="col-6"><img src="/img/loyalty2-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline2">Offer your customers with in-app mobile coupons </p>
          </div>
        </div>
      </div>

      <div class="row grow">
        <div class="col-6"><img src="/img/location-based-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline2">Send relevant messages for a pre-defined location or geographic area.</p>
          </div>
        </div>
        <div class="col-6"><img src="/img/pyze-icon.png" alt="">
          <div class="row">
            <p class="col-12 grey subline2">Detailed analytics and reporting</p>
          </div>
        </div>
      </div>
    </div>
    <button onclick="orderLink()" class="btn btn-secondary blurple-bg center moveDown">BUILD YOUR APP NOW
      &#8594;</button>
  </div>
  <div class="blurple-bg">
    <div class="container text-center erie-text">
      <i style="font-size: 50px;" class="fas fa-quote-left text-center"></i>
      <h3 class="light-blue">Customers use our app to order their replacement parts, accurately, promptly, and most
        importantly, in real time!!! The app Works!</h3><br>
      <p class="white">BILLY KEAN FROM ERIE VEHICLE, EST 1917</p>
      <img src="/img/erie-logo.png" />

    </div>
  </div>

  </div>

  <?php include_once('../includes/footer.php'); ?>
  <script src="/js/vendor/jquery-3.3.1.min.js"></script>
  <script src="/js/vendor/popper.min.js"></script>
  <script src="/js/vendor/bootstrap.min.js"></script>
  <script src="/js/main.js"></script>
</body>

</html>