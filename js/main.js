function nav() {
    document.body.innerHTML += '<div class="container  border-bottom "><nav class="navbar navbar-expand-lg row navbar navbar-light"><div class="col col-4"><a class="navbar-brand" href="index.html"><img src="img/logo.png"></a></div><button class="navbar-toggler"type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button><div class="col col-4"></div><div class="collapse navbar-collapse col col-4 right" id="navbarNav"><ul class="navbar-nav grey"><li class="nav-item active"><a class="nav-link" href="#">About <span class="sr-only">(current)</span></a></li><li class="nav-item"><a class="nav-link" href="pricing.html">Price</a></li><li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li><li class="nav-item"><a class="nav-link"  target="_blank" href="https://apps.mobilesoft.com/Login.aspx">Login</a></li></ul></div></div></nav>'
};

function footer() {
    document.body.innerHTML += '<div class="row footer-copyright container-fluid"> <div class = "col col-4 white" ><img class = "logo"src = "img/logo.png"alt = "" ><p > We build custom mobile apps that allow business owners to: ENGAGE, INTERACT & RETAIN your customer baseon the most important device they own...their smartphone. </p> <p > Call us today to learn how MobileSoft can change your business: <a href = "tel:8887148014" > 888 - 714 - 8014 </a></p></div> <div class = "col col-8" ><!-- <p class="center">2019 Mobilesoft. All Rights Reserved.</p> --><div class = "row white" ><div class = "col col-3" ><hr class = "short green-gradient" ><h5> About Us </h5> <a href = "#" > Studio </a><br/ ><a href = "#" > Technology </a> </div> <div class = "col col-3" ><hr class = "short blue-gradient" ><h5 > Help Center </h5> <a href = "#" > Terms of Use </a><br/ ><a href = "#" > Privacy Policy </a> </div> <div class = "col col-3" ><hr class = "short yellow-gradient" ><h5 > Our Work </h5> <a href = "#" > Bail Industry </a><br/ ><a href = "#" > Automotive </a><br/ ><a href = "#" > Law Firms </a><br/ ><a href = "#" > Restaurant </a><br/ ></div> <div class = "col col-3" ><hr class = "short pink-gradient" ><h5 > Contact Us </h5> <a href = "#" > Get In Touch </a><br/ ><a href = "#" > Support </a><br/ ><a href = "#" > Client Login </a> </div></div> </div> </div>'
};

function changePhone() {
    var elementName = event.target.id;
    console.log(elementName);
    switch (elementName) {
        case "push-notification":
            console.log("image changed to push-notification");
            document.getElementById("frame").src = "img/push-notifications.png";
            break;
        case "geofence":
            console.log("image changed to geofence");
            document.getElementById("frame").src = "img/geo-fencing.png";
            break;
        case "intake":
            console.log("image changed to intake");
            document.getElementById("frame").src = "img/intake-forms.png";
            break;
        case "loyalty":
            console.log("image changed to loyalty");
            document.getElementById("frame").src = "img/loyalty.png";
            break;
        default:
            console.log("broke");
    }
}

function orderLink() {
    window.open("https://webtest.mobilesoft.com/order.html", "_self")
}
//Industry image changer
function industrySelector() {
    var elementName = event.target.id;
    console.log(elementName);
    switch (elementName) {
        //Bail Page
        case "bail-out":
            document.getElementById("frame").src = "img/Features-App-BailBonds.png";
            break;
        case "bail-check-in":
            document.getElementById("frame").src = "img/Features-App-Checkin.png";
            break;
        case "e-sign-attorney":
            document.getElementById("frame").src = "img/feature-esign-law.png";
            break;
            //Mobile Payments
        
        case "mobile-payments-attorney":
            document.getElementById("frame").src = "img/feature-paymentx-law.png";
            break;
        case "mobile-payments-auto":
            console.log("image changed to geofence");
            document.getElementById("frame").src = "img/geo-fencing.png";
            break;
            //My Garage
        case "my-garage":
            console.log("image changed to intake");
            document.getElementById("frame").src = "img/intake-forms.png";
            break;
            //Mobile Ordering
        case "mobile-ordering":
            console.log("image changed to loyalty");
            document.getElementById("frame").src = "img/feature-loyalty.png";
            break;
            //LOYALTY
        case "loyalty-coupons-restaurant":
            console.log("image changed to loyalty");
            document.getElementById("frame").src = "img/feature-mobile-ordering.png";
            break;
        default:
            console.log("broke");
    }
}

// on mobile change header and footer links to point to mobile page

if ($('.navbar-toggler').is(":visible")) {
    $('.header-logo').attr("href", "/mobile/index.html");
    $('.header-about').attr("href", "/mobile/about-us.html");
    $('.header-pricing').attr("href", "/mobile/pricing.html");
    $('.header-contact').attr("href", "/mobile/contact.html");
}

// contact us
if ($('#contact-us-form').length) {
    $('#contact-us-form').parsley({
        errorClass: 'is-invalid',
        successClass: 'is-valid',
        errorsWrapper: '<span class="invalid-feedback"></span>',
        errorTemplate: '<div></div>'
    });
};