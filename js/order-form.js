var $carousel = $('.main-carousel');
var isFlickity = false;

// ORDER FORM FUNCTIONS
var orderForm = (function () {

  // Navigation Steps
  var $stepNavAll = $('.sidebar-step');
  var $stepNav1 = $('.step-1');
  var $stepNav2 = $('.step-2');
  var $stepNav3 = $('.step-3');

  // Page Steps
  var $pageAll = $('.pages');
  var $page1 = $('.page-1');
  var $page2 = $('.page-2');
  var $page3 = $('.page-3');

  // Step Indicator Boxes
  var $stepBoxAll = $('.step-box');
  var $stepBox1 = $('.step-box-1');
  var $stepBox2 = $('.step-box-2');
  var $stepBox3 = $('.step-box-3');

  // Step Tips
  var $tipsAll = $('.tips');
  var $tip1 = $('.tip-1');
  var $tip2 = $('.tip-2');
  var $tip3 = $('.tip-3');

  // Navigation Buttons
  var $resumeBtn = $('.resume-btn');
  var $beginBtn = $('.begin-btn');
  var $goTo1Btn = $('.step-1-btn');
  var $goTo2Btn = $('.step-2-btn');
  var $goTo3Btn = $('.step-3-btn');

  // Handle button presses
  $resumeBtn.on('click', function () { resumeOrder(); });
  $beginBtn.on('click', function () { beginOrder(); });
  $goTo1Btn.on('click', function () { goToPage1(); });
  $goTo2Btn.on('click', function () { goToPage2(); });
  $goTo3Btn.on('click', function () { goToPage3(); });

  // Handle navigation presses
  $stepNav1.on('click', function () { goToPage1(); });
  $stepNav2.on('click', function () { goToPage2(); });
  $stepNav3.on('click', function () { goToPage3(); });

  // Handle box presses
  $stepBox1.on('click', function () { goToPage1(); });
  $stepBox2.on('click', function () { goToPage2(); });
  $stepBox3.on('click', function () { goToPage3(); });

  // Resume Order
  var resumeOrder = function () {
    if (validateResume()) {
      getResumeData();
    }
  }

  // Begin new order
  var beginOrder = function () {
    if (validateStep0()) {
      saveWelcomeStep();
      goToPage1();
    }
  }

  // Go to Page 1
  var goToPage1 = function () {
    if (validateStep0()) {
      clearSteps();
      socialCheck();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
      $page1.removeClass('d-none');
      $tip1.removeClass('d-none');
      $stepNav1.removeClass('inactive-step').addClass('active-step');
      $stepBox1.removeClass('step-incomplete').addClass('step-complete');
      if (isFlickity) $carousel.flickity('destroy');
      initCarousel();
    }
  }

  // Go to Page 2
  var goToPage2 = function () {
    if (validateStep1()) {
      saveStep1();
      clearSteps();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
      $page2.removeClass('d-none');
      $tip2.removeClass('d-none');
      $stepNav2.removeClass('inactive-step').addClass('active-step');
      $stepBox1.removeClass('step-incomplete').addClass('step-complete');
      $stepBox2.removeClass('step-incomplete').addClass('step-complete');
    }
  }

  // Go to Page 3
  var goToPage3 = function () {
    if (validateStep2()) {
      saveStep2();
      clearSteps();
      showSummary();
      chargebeeData();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
      $page3.removeClass('d-none');
      $tip3.removeClass('d-none');
      $stepNav3.removeClass('inactive-step').addClass('active-step');
      $stepBox1.removeClass('step-incomplete').addClass('step-complete');
      $stepBox2.removeClass('step-incomplete').addClass('step-complete');
      $stepBox3.removeClass('step-incomplete').addClass('step-complete');
    }
  }

  // Clear all step classes
  var clearSteps = function () {
    $pageAll.addClass('d-none');
    $tipsAll.addClass('d-none');
    $stepNavAll.removeClass('active-step').addClass('inactive-step');
    $stepBoxAll.removeClass('step-complete').addClass('step-incomplete');
  }

  // get resume data for form
  function getResumeData() {

    var resumeEmail = $("#resume-email").val();
    $.ajax({
      type: 'POST',
      url: 'functions/resumeOrder.php',
      data: { "email": resumeEmail },
      dataType: 'json',
      error: function (req, err) {
        //console.log(err);
      }
    }).done(function (data) {
      if (data.status === 'success') {
        fillFields(data);
        resumeStep0();
        //if (data.step_number === '0') resumeStep0();
        // if (data.step_number === '1') goToPage1();
        // if (data.step_number === '2') goToPage2();
        // if (data.step_number === '3') goToPage3();
      } else {
        orderNotFound(resumeEmail);
      }
    });

  };
})();

// Order form
var $orderForm = $("#order-form");

// FORM VALIDATION FUNCTIONS
$orderForm.parsley({
  errorClass: 'is-invalid',
  successClass: 'is-valid',
  errorsWrapper: '<span class="invalid-feedback"></span>',
  errorTemplate: '<div></div>'
})
function resetValidation() { $orderForm.parsley().reset(); }
function validateResume() { return ($orderForm.parsley().validate({ group: 'resume' })) ? true : false; }
function validateStep0() { return ($orderForm.parsley().validate({ group: 'page-0' })) ? true : false; }
function validateStep1() { return ($orderForm.parsley().validate({ group: 'page-1' })) ? true : false; }
function validateStep2() { return ($orderForm.parsley().validate({ group: 'page-2' })) ? true : false; }

// Save data after completing the welcome step
function saveWelcomeStep() {
  $.ajax({
    type: 'POST',
    url: 'functions/saveProspect.php',
    data: ({
      "first_name": $("#first-name").val(),
      "last_name": $("#last-name").val(),
      "email": $("#email").val(),
      "phone": $("#phone").val()
    }),
    success: function (response) {
      //console.log(response);
    }
  });
}

// Save data after completing step 1
function saveStep1() {
  if ($('input#need-logo').is(':checked')) needChecked = 1;
  else needChecked = 0;

  $.ajax({
    type: 'POST',
    url: 'functions/step1.php',
    data: ({
      "template_name": $('#template-name').val(),
      "template_id": $('#template-id').val(),
      "logo_url": $('#logo_url').val(),
      "need_logo": needChecked,
      "business_website": $('#business_website').val(),
      "youtube_url": $('#youtube_url').val(),
      "instagram_url": $('#instagram_url').val(),
      "yelp_url": $('#yelp_url').val(),
      "facebook_url": $('#facebook_url').val(),
      "twitter_url": $('#twitter_url').val(),
      "linkedin_url": $('#linkedin_url').val(),
      "email": $("#email").val(),
      "phone": $("#phone").val()
    }),
    success: function (response) {
      //console.log(response);
    }
  });
}

// Save data after completing step 2
function saveStep2() {
  $.ajax({
    type: 'POST',
    url: 'functions/step2.php',
    data: ({
      "business_name": $('#business_name').val(),
      "business_address": $('#business_address').val(),
      "business_city": $('#business_city').val(),
      "business_state": $('#business_state').val(),
      "business_zip": $('#business_zip').val(),
      "business_phone": $('#business_phone').val(),
      "email": $("#email").val(),
      "phone": $("#phone").val()
    }),
    success: function (response) {
      //console.log(response);
    }
  });
}

// Show order summary
function showSummary() {
  socialCheck();
  $('.summary-company').html($('#business_name').val());
  $('.summary-address').html($('#business_address').val());
  $('.summary-address-2').html($('#business_city').val() + ', ' + $('#business_state').val() + ' ' + $('#business_zip').val());
  $('.summary-phone').html($('#business_phone').val());
  $('.summary-email').html($("#email").val());
  $('.summary-website').html($('#business_website').val());
  $('.summary-template img').attr("src", 'img/full/' + $('#template-name').val() + '.jpg');
}

// RESUME ORDER FUNCTIONS
// order not found
function orderNotFound(email) {
  var notFoundMessage = '<p>Could not find an unfinished order for ' + email + '</p>';
  $('#notFoundModal').modal('show').find('.modal-body').html(notFoundMessage);
  $('#notFoundMessage').text('Could not find an unfinished order for '.email);
  $("#resume-email").val('');
}

// resume order at step 0
function resumeStep0() {
  $("#resume-email").val('');
  $("#step-0-button").html('Resume Order');
  $("#order-message-0").html('Resume Order');
}

// add resume data to form
function fillFields(fieldData) {
  resetValidation();
  $('html, body').animate({ scrollTop: 0 }, 'fast');
  for (var fieldID in fieldData) {
    if (fieldData[fieldID] != '') {
      if (fieldID === 'logo_name') showLogo(fieldData[fieldID], fieldData['logo_size']);
      else if (fieldID === 'youtube_url'
        || fieldID === 'instagram_url'
        || fieldID === 'yelp_url'
        || fieldID === 'facebook_url'
        || fieldID === 'twitter_url'
        || fieldID === 'linkedin_url') {
        $('#' + fieldID + '-check .sm-chk:checkbox').prop('checked', true);
        $("#" + fieldID).val(fieldData[fieldID]);
      }
      else if (fieldID === 'need_logo' && fieldData[fieldID] == 1) $('input#need-logo').prop('checked', true);
      else $("#" + fieldID).val(fieldData[fieldID]);
    }
  }
}

// SOCIAL MEDIA
$(".image-checkbox").on("click", function (e) { socialCheck(); }); // also executes on opening of page 1

function socialCheck() {
  var smLinks = ['youtube', 'instagram', 'yelp', 'facebook', 'twitter', 'linkedin'];
  // reset summary social media images
  $('.summary-image').remove();
  var a = 1;
  for (var i = 0; i < smLinks.length; i++) {
    if ($('#' + smLinks[i] + '_url-check .sm-chk:checkbox').prop("checked")) {
      $('#' + smLinks[i] + '_url-input').removeClass('d-none');
      $('#' + smLinks[i] + '_url-check').removeClass('image-checkbox-checked');
      // display social media images on summary page
      if ($('#' + smLinks[i] + '_url').val() !== '') $('.social-' + a).html('<img class="img-fluid summary-image" src="img/sm-' + smLinks[i] + '-color.png">')
      a++;
    } else {
      $('#' + smLinks[i] + '_url-input').addClass('d-none');
      $('#' + smLinks[i] + '_url-check').addClass('image-checkbox-checked');
      $('#' + smLinks[i] + '_url').val(''); // on deselect remove url
    }
  }
}

// TEMPLATE CAROUSEL
var initCarousel = function () {
  isFlickity = true;
  $carousel.flickity({
    wrapAround: true,
    imagesLoaded: true,
    pageDots: false,
    fullscreen: true,
    dragThreshold: 10,
    arrowShape: { x0: 15, x1: 60, y1: 45, x2: 60, y2: 40, x3: 60 }
  });
  // remember selected template on re-init
  $id = $('input[name="template-id"]').val();
  if ($id != "") $carousel.flickity('select', $id, false, true);
}

// Template Selection on settle
$carousel.on('settle.flickity', function (event, index) {
  templateName = $('.carousel-cell.is-selected').attr('title');
  console.log('Name: ' + templateName);
  setTemplateFields(index, templateName);
});

// Exit fullscreen on click of exit button
$('.flickity-fs-button').click(function () {
  $carousel.flickity('exitFullscreen');
  $carousel.flickity('resize');
});

// Template Selection on click
$carousel.on('staticClick.flickity', function (event, pointer, cellElement, cellIndex) {

  if ($('.is-fullscreen').length > 0) {
    $('.carousel-cell').removeClass('fs');
    $carousel.flickity('exitFullscreen');
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    $carousel.flickity('resize');
  } else {
    $('.carousel-cell').addClass('fs');
    $carousel.flickity('viewFullscreen');
  }

  // Change slide and get values for hidden fields
  if (typeof cellIndex == 'number') {
    $carousel.flickity('selectCell', cellIndex);
    templateName = $(pointer.path[0]).attr('alt');
    setTemplateFields(cellIndex, templateName);
  }
});

// Set hidden field values
var setTemplateFields = function (index, name) {
  $('input[name="template-name"]').val(name);
  $('input[name="template-id"]').val(index);
}

// Template Selection on settle
$('#previewModal').on('settle.flickity', function (event, index) {
  templateName = $('.carousel-cell.is-selected picture > img').attr('alt');
  $carousel.flickity('select', index, false, true);
  setTemplateFields(index, templateName);
});

$carousel.on('fullscreenChange.flickity', function (e, isFullscreen) {
  window.location.hash = "order";
  if (isFullscreen) {
    $('.carousel-cell').addClass('fs');
    $('.flickity-fs-button').removeClass('d-none');
    $('html').addClass('overflow-hidden h-100');
    $('body').addClass('overflow-hidden h-100');
  } else {
    $('.carousel-cell').removeClass('fs');
    $('.flickity-fs-button').addClass('d-none');
    $('html').removeClass('overflow-hidden h-100');
    $('body').removeClass('overflow-hidden h-100');
  }
});

$(window).on('hashchange', function (event) {
  if (window.location.hash != "#order") {
    $carousel.flickity('exitFullscreen');
    $carousel.flickity('resize');
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }
});

// UPLOAD LOGO
function uploadLogo() {
  $('#upload-error').addClass('d-none');
  $('.progress').removeClass('d-none');

  var logo = $('#upload-logo')[0].files[0];

  if (logo.size <= 8388608) {

    var formData = new FormData();
    formData.append('file', logo);
    formData.append('email', $("#email").val());
    formData.append('phone', $("#phone").val());
    formData.append('template_name', $('#template-name').val());
    formData.append('template_id', $('#template-id').val());

    $.ajax({
      url: 'functions/uploadLogo.php',
      type: 'post',
      data: formData,
      processData: false,
      contentType: false,
      xhr: function () {
        var xhr = new window.XMLHttpRequest();
        //Upload progress
        xhr.upload.addEventListener("progress", function (evt) {
          if (evt.lengthComputable) {
            var percentComplete = (evt.loaded / evt.total * 100).toFixed(0);
            $('.progress-bar').width(percentComplete + '%');
          }
        }, false);
        return xhr;
      },
      success: function (response) {
        $('.progress').addClass('d-none');
        $('.progress-bar').width('0%');
        if (response.status === 'success') {
          showLogo(response.fileName, response.fileSize);
        } else {
          $('#upload-error').removeClass('d-none');
          $('#upload-error').html('WARNING: ' + response.error);
        }
      },
    });
  } else {
    $('#upload-error').removeClass('d-none');
    $('#upload-error').html('WARNING: File is too large');
  }

};

// show logo
function showLogo(logoName, logoSize) {

  logoExtension = logoName.split('.').pop();
  cleanedLogoName = logoName.split('-').pop();

  if (logoExtension === 'pdf') previewSrc = 'img/pdf-icon.png';
  else if (logoExtension === 'psd') previewSrc = 'img/psd-icon.png';
  else if (logoExtension === 'ai') previewSrc = 'img/ai-icon.png';
  else if (logoExtension === 'tif' || logoExtension === 'tiff') previewSrc = 'img/tiff-icon.png';
  else previewSrc = 'incomplete/' + logoName;

  // Hide upload button
  $('#upload-logo-button').addClass('d-none');
  $('.logo-format').addClass('d-none');

  // Show logo preview
  $('.logo-info').removeClass('d-none');
  $('.logo-preview img').attr('src', previewSrc);

  // Show logo name
  $("#upload-file-name").html("Name: " + cleanedLogoName);
  $("#upload-file-size").html("Size: " + formatBytes(logoSize));

  // Hide Logo URL
  $(".logo-url").addClass('d-none');

  // Hide don't have logo
  $(".logo-checkbox").addClass('d-none');
};

// remove logo
$('.remove-logo').on('click', function () {

  $.ajax({
    type: 'POST',
    url: 'functions/removeLogo.php',
    data: ({
      "email": $("#email").val(),
      "phone": $("#phone").val()
    }),
    success: function (response) {
      if (response.status === 'success') {
        // Show upload button
        $('#upload-logo-button').removeClass('d-none');
        $('.logo-format').removeClass('d-none');

        // Hide logo preview
        $(".logo-info").addClass('d-none');
        $(".logo-preview img").attr("src", '');

        // Show logo name
        $("#upload-file-name").html("");
        $("#upload-file-size").html("");

        // Show logo URL
        $(".logo-url").removeClass('d-none');

        // Show don't have logo
        $(".logo-checkbox").removeClass('d-none');
      } else {
        // console.log(response);
      }
    },
  });


});

// format image file size extension
function formatBytes(a, b) { if (0 == a) return "0 Bytes"; var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB"], f = Math.floor(Math.log(a) / Math.log(c)); return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f] }

// chargeBee
function chargebeeData() {

  var cbInstance = Chargebee.getInstance();
  var cart = cbInstance.getCart();
  var link = document.querySelectorAll("[data-cb-type=checkout]")[0];
  var product = cbInstance.getProduct(link);

  var customer = {
    first_name: $("#first-name").val(),
    last_name: $("#last-name").val(),
    email: $("#email").val(),
    phone: $("#phone").val(),
    company: $('#business_name').val(),
    cf_company_address: $('#business_address').val() + ', ' + $('#business_city').val() + ', ' + $('#business_state').val() + ' ' + $('#business_zip').val(),
    cf_company_website: addhttp($('#business_website').val())
  };

  // need logo checkbox
  if ($('input#need-logo').is(':checked')) needChecked = 'True';
  else needChecked = '';

  // uploaded logo
  rawLogo = $("#upload-file-name").text();
  if (rawLogo === '') uploadedLogo = '';
  else uploadedLogo = 'https://webtest.mobilesoft.com/uploaded_logos/' + rawLogo.split(': ').pop();

  var subscription = {
    cf_template_name: $('#template-name').val(),
    cf_uploaded_logo: addhttp(uploadedLogo),
    cf_logo_url: addhttp($('#logo_url').val()),
    cf_needs_logo: needChecked,
    cf_youtube: addhttp($('#youtube_url').val()),
    cf_instagram: addhttp($('#instagram_url').val()),
    cf_yelp: addhttp($('#yelp_url').val()),
    cf_facebook: addhttp($('#facebook_url').val()),
    cf_twitter: addhttp($('#twitter_url').val()),
    cf_linkedin: addhttp($('#linkedin_url').val())
  }

  // Setting custom fields
  cart.setCustomer(customer);
  product.setCustomData(subscription);

  cbInstance.setCheckoutCallbacks(function (cart) {
    // custom callbacks based on cart object
    return {
      loaded: function () {
        console.log("checkout opened");
      },
      close: function () {
        console.log("checkout closed");
      },
      success: function () {
        console.log("success!");
      },
      step: function (value) {
        // value -> which step in checkout
        console.log(value);
      }
    }
  });
}

// prepend http if does not have http or https already. if empty return empty
function addhttp(url) {
  if (url === '') return '';
  if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
    url = "http://" + url;
  }
  return url;
}
