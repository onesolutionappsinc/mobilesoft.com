<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mobilesoft.com</title>
    <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styles/vendor/slick.css" />
    <link rel="stylesheet" type="text/css" href="styles/vendor/slick-theme.css" />
    <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
    <link rel="stylesheet" href="styles/fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="styles/styles.css" />
    <link rel="stylesheet" type="text/css" href="styles/vendor/plyr.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="styles/industry.css" />
</head>

<body>
    <?php include_once('includes/header.php'); ?>

    <div class="container-fluid">
        <!--Split left right-->
        <div class="row bail-upper">
            <div class="col-4"></div>
            <div class="col-4 text-center">
                <img class="img-fluid pt-lg-5 pt-3 pt-md-4 pb-lg-5 pb-3 pb-md-4 mb-lg-3"
                    src="img\top-banner-iphone-bail.png" alt="">
            </div>
            <!-- <div class="col-4 my-auto">
                <h1 class="text-left">Get your<br>Mobile app<br>for your Agency</h1>
            </div> -->
        </div>
    </div>

    <div class="d-flex align-items-center justify-content-between px-4 border-left border-right center" style="width: 75%;">
        <div class="">PROUD PARTNERS OF:</div>
        <div class="border-left border-right"><img class="img-fluid pl-4 pr-5" src="img/partners-pbus.jpg" alt=""></div>
        <div class=""><img class="img-fluid" src="img/partners-bailbonds.jpg" alt=""></div>
        <div class="border-left"><img class="img-fluid" src="img/partners-capitra.jpg" alt=""></div>
    </div>

    <div class="row">
        <div class="container-fluid text-center pt-3 video-bg m-0 p-0">
            <h1 class="text-white slimmer moveDown" style="font-size: 80px;">Why a mobile app?</h1>
            <br>
            <div class="video-frame">
                <video poster="/img/bail-video-cover.jpg" id="player" playsinline controls>
                    <source src="/video/mobilesoft-bail-app-video.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="play-video-text">
                <img class="img-fluid" src="img/play-video-label.png" alt="">
            </div>
        </div>
    </div>

    <div class="row blurple-bg">
        <div class="d-flex mb-0 mt-2 py-0 app-benefits">
            <img class="img-fluid align-self-end" src="img/phone-bailbonds" alt="">
            <div class="align-self-start benefit pl-5">
                <h2 class="light-blue bold" style ="font-size: 89px;">Mobile App Benefits</h2>
                <div class="inner-benefit-text">
                    <h4 class="white bold">We are the #1 mobile app builder in the Bail Bonds Service Industry.</h4>
                    <p class ="light-grey slimmer">We know the bail bonds service industry and have created a unique set of features specifically for
                        shop
                        owners. Our philosopy: <br><span>Your Agency. Your App.</span></p>
                </div>
                <button type="button" class="btn light-blue-bg long-btn center btn-secondary uppercase" style="margin: 10% 0 0;">start your build now
                    <span>&#8594;<span></button>
            </div>
           
        </div>
    </div>



    <div class="md-blue-grad">
        <div class="bail-selector my-5">
            <div class="text-center mb-0">
                <h3>APP FEATURES</h3>
            </div>
            <div class="d-flex">
                <div class="text-center e-sign pr-2">
                    <span id="bail-out"  onmouseover="industrySelector()">
                        <img class="img-fluid w-50" src="img/features-bail-out-icon.png" alt="" id="bail-out"
                        onmouseover="industrySelector()">
                        <h2 class="light-blue">Bail Out</h2>
                        <div class="light-grey">Your clients can use your app to instantly notify you of their arrest, location, and delivery.
                        </div>
                    </span>
                </div>
                <div class="text-center phone">
                    <img id="frame" class="img-fluid" src="img/Features-App-Phone.png" alt="">
                </div>
                <div class="text-center mobile-payments pl-2">
                    <span id="bail-check-in"  onmouseover="industrySelector()">
                        <img class="img-fluid w-50" src="img/features-bail-gps-icon.png" alt=""
                            id="bail-check-in"  onmouseover="industrySelector()">
                        <h2 class="light-blue">GPS Check In </h2>
                        <div class="light-grey">Feature that records time, date, and location automatically*. Keep the phone lines open for new business
                            
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div class="special-features text-center">
            <h1 class="col light-blue mb-4">Additional Special Features</h1>
            <div class="row">
                <div class="col white-box text-center">
                    <img src="img/icon-call.png" alt="">
                    <p class="bold">Call</p>
                    <p class="light-grey">
                        Allow customers to contact you in one touch.
                    </p>
                </div>
                <div class="col white-box text-center">
                    <img src="img/icon-directions.png" alt="">
                    <p class="bold">Directions</p>
                    <p class="light-grey">
                        GPS directions allows the customer to locate your firm and plan the quickest route to your firm.
                    </p>
                </div>
                <div class="col white-box text-center">
                    <img src="img/icon-bail-out.png" alt="">
                    <p class="bold">Bail Out</p>
                    <p class="light-grey">
                        Your clients can use your app to instantly notify you of their arrest, location, and delivery.
                    </p>
                </div>
                <div class="col white-box text-center">
                    <img src="img/icon-gps.png" alt="">
                    <p class="bold">GPS Checking</p>
                    <p class="light-grey">
                        GPS check in feature that records time, dates, and location automatically* Keep the phone lines
                        open for new
                        business
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col white-box text-center">
                    <img src="img/icon-accicent-cam.png" alt="">
                    <p class="bold">Bail Cam</p>
                    <p class="light-grey">
                        Collect even more information on each defendant with this easy to use feature
                    </p>
                </div>
                <div class="col white-box text-center">
                    <img src="img/icon-ask-agent.png" alt="">
                    <p class="bold">Ask the Agent</p>
                    <p class="light-grey">
                        Integrated directory of all your bail agents and easy access for clients to get important
                        questions answered
                    </p>
                </div>
                <div class="col white-box text-center">
                    <img src="img/icon-court-apperance.png" alt="">
                    <p class="bold">Call</p>
                    <p class="light-grey">
                        Allow your clients to check-in to court and submit paperwork in one easy step
                    </p>
                </div>
                <div class="col white-box text-center">
                    <img src="img/icon-location.png" alt="">
                    <p class="bold">Call</p>
                    <p class="light-grey">
                        Bail Agents have the ability to perform a defendant search once the client has missed their
                        court date.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid d-flex banner-bail testimonial justify-content-between my-0 py-0">
        <div class="align-self-end app-preview"><img class="img-fluid px-lg-3 px-md-3 px-1"
                src="img/testimonial-phones.png"></div>
        <div class="text-center quote align-self-center py-3">
            <div>
                <span class="outer-line align-middle"></span>
                <i style="font-size: 50px;" class="fas fa-quote-left light-blue text-center align-middle px-2"></i>
                <span class="outer-line align-middle"></span>
            </div>
            <div class="white pt-3">By integrating this app into my business it has given me ease of mind when my
                clients
                attend
                court by giving them the ability to take photos of their continuance order, receipts for payments or
                even
                disposition papers and allowing them to submit it directly through the app
            </div>
            <div class="light-blue bold pt-4"> Chris Lucaro</div>
            <div class="white">Owner/Operator</div>
            <div class="white bold">A-Abailable Bail Bonds</div>
        </div>
        <div class="align-self-end avatar"><img class="img-fluid pl-lg-2" src="img/testimonial-person-bail.png">
        </div>
    </div>


    <div class="white-bg my-5 py-3">
        <div class="text-center mb-4">
            <h4 class="light-blue">Some of our Loyal Customers</h4>
        </div>
        <div class="d-flex justify-content-center">
            <div class="customer p-4 border">Client 1</div>
            <div class="customer p-4 border border-left-0">Client 2</div>
            <div class="customer p-4 border border-left-0">Client 3</div>
            <div class="customer p-4 border border-left-0">Client 4</div>
            <div class="customer p-4 border border-left-0">Client 5</div>
            <div class="customer p-4 border border-left-0">Client 6</div>
        </div>
    </div>

    <?php include_once('includes/footer.php'); ?>
    <script src="js/vendor/jquery-3.3.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/vendor/plyr.js"></script>
    <script>
        const player = new Plyr('#player');
    </script>
</body>

</html>