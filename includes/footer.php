<footer>
  <div class="container-fluid mb-5">
    <div class="row">
      <div class="text-center center dark-blue-image col pb-2 p-md-5">
        <h1 class="p-2 py-md-0 mx-md-5 px-md-5">Start building out your Mobile app today for your business</h1>
        <p class="light-grey px-3 px-md-0 pt-md-3 pb-md-4">Update your app in real-time with new content and products
        </p>
        <button onclick="orderLink()" class="center btn btn-secondary white light-blue-bg mb-2 mb-md-0 py-md-2">SIGN UP
          NOW <span class="arrow">→<span></span></span></button>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-text">
          <div class="footer-logo text-center"><img src="/img/logo-white.png" alt="Footer Logo Trademark White"></div>
          <p class="d-none d-md-block">We build custom mobile apps that allow business owners to: ENGAGE, INTERACT &amp;
            RETAIN your
            customer base on the most important device they own...their smartphone.</p>
        </div>
      </div>
      <div class="col-md-8 col-12 text-center text-md-left">
        <div class="row white">
          <div class="col-md-3">
            <hr class="short green-gradient d-none d-md-block text-left align-bottom">
            <h5 class="text-nowrap"><a>About Us</a></h5>
            <ul class="footer-nav d-none d-md-block">
              <li><a href="">Studio</a></li>
              <li><a href="">Technology</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <hr class="short blue-gradient d-none d-md-block text-left align-bottom">
            <h5 class="text-nowrap"><a>Help Center</a></h5>
            <ul class="footer-nav d-none d-md-block">
              <li><a href="">Terms of Use</a></li>
              <li><a href="/privacy-policy.html">Privacy Policy</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <hr class="short yellow-gradient d-none d-md-block text-left align-bottom">
            <h5 class="text-nowrap"><a>Our Work</a></h5>
            <ul class="footer-nav d-none d-md-block">
              <li><a href="">Bail Industry</a></li>
              <li><a href="">Automotive</a></li>
              <li><a href="">Law Firms</a></li>
              <li><a href="">Restaurant</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <hr class="short pink-gradient d-none d-md-block text-left align-bottom">
            <h5 class="text-nowrap"><a>Contact Us</a></h5>
            <ul class="footer-nav d-none d-md-block">
              <li><a href="">Get In Touch</a></li>
              <li><a href="/support.html">Support</a></li>
              <li><a href="https://apps.mobilesoft.com/login.aspx" target="_blank" rel="noopener noreferrer">Client
                  Login</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="d-flex align-items-center justify-content-between pt-5 footer-logos">
      <div class="px-1 px-md-3"><a
          href="https://www.bbb.org/minnesota/business-reviews/mobile-apps/mobilesoft-technology-inc-in-minneapolis-mn-1000016542/#sealclick"><img
            class="img-fluid" src="/img/bbb-logo.png" alt="Mobilesoft Technology, Inc. BBB Business Review"></a></div>
      <div class="px-1 px-md-3"><img class="img-fluid" src="/img/chamber-logo.png" alt=""></div>
      <div class="px-1 px-md-3"><img class="img-fluid" src="/img/logo-AMA.png" alt=""></div>
      <div class="px-1 px-md-3"><img class="img-fluid" src="/img/white-play-logo.png" alt=""></div>
      <div class="px-1 px-md-3"><img class="img-fluid" src="/img/white-app-store-logo.png" alt=""></div>
    </div>
  </div>
</footer>