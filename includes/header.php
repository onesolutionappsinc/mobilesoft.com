<header>
  <div class="container">
    <nav class="navbar navbar-expand-md row navbar navbar-light">
      <a class="navbar-brand header-logo" href="/index.html"><img src="/img/logo.png"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><i
          class="fas fa-bars light-blue"></i></button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto grey">
          <li class="nav-item"><a class="nav-link header-about" href="/about-us.html">About<span
                class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item"><a class="nav-link header-pricing" href="/pricing.html">Price</a></li>
          <li class="nav-item"><a class="nav-link header-contact" href="/contact.html">Contact</a></li>
          <li class="nav-item"><a class="nav-link" target="_blank"
              href="https://apps.mobilesoft.com/Login.aspx">Login</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>