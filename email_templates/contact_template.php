<?php
    
    // Internal
    $from_address = 'donotreply@mobilesoft.com';
    $from_name = 'MobileSoft Website';
    $from_subject = 'MobileSoft.com Contact Request';
    $internal_plaintext = <<<TEXTMESSAGE
Hello %internal_name%,
\r\n
\r\nYou recieved a contact request from the MobileSoft website, their information is as follows:
\r\nName: %contact_name%,
\r\nCompany Name: %contact_company%,
\r\nPhone Number: %contact_number%,
\r\nEmail: %contact_email%,
\r\nExisting Client: %contact_existing%,
\r\nMessage: %contact_message%,
\r\n
\r\nThanks,
\r\n
\r\nYour friendly superintellegent AI website
\r\n
    TEXTMESSAGE;

    $internal_html = <<<HTMLMESSAGE
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:200&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <style>
    @import url('https://fonts.googleapis.com/css?family=Work+Sans:200');
    html {
        font-family: 'Work Sans', sans-serif;
        color: grey;
    }
    body {
        margin: 0;
        padding: 0;
    }
    .page {
        background-color: white;
        position: relative;
    }
    .container {
        margin: auto;
        padding: 5%;
        max-width: 750px;
    }
    p {
        line-height: 1.5em;
        letter-spacing: .2px;
    }
    .address {
        font-weight: bold;
        color: #8d8d8d;
        margin-top: 10%;
        text-align: center;
    }
    .header-img {
        width: 50%;
        margin-top: 1em;
        margin-bottom: 3em;
    }
    img {
        width: 100%;
        display: block;
    }
    </style>
    <div class="page">
        <div class="container">
            <div class="header-img">
            <img src="https://webtest.mobilesoft.com/img/logo.png" />
            </div>
            <p>Hello %contact_name%,</p>

            <p>You recieved a contact request from the MobileSoft website, their information is as follows:</p>

            \r\nName: %contact_name%,
            \r\nCompany Name: %contact_company%,
            \r\nPhone Number: %contact_number%,
            \r\nEmail: %contact_email%,
            \r\nExisting Client: %contact_existing%,
            \r\nMessage: %contact_message%,

            <p><b>Name</b>: %contact_name%</p>
            <p><b>Company Name</b>: %contact_company%</p>
            <p><b>Email</b>: ${validatedCleanRequest.email}</p>
            
            <p><b>Phone Number</b>: ${validatedCleanRequest.phone}</p>
            <p><b>Options</b>: ${validatedCleanRequest.options}</p>
            <p><b>Existing Client</b>: ${validatedCleanRequest.existingClient}</p>
            <p><b>Price Range</b>: ${validatedCleanRequest.priceRange}</p>
            <p><b>Message</b>: ${validatedCleanRequest.message}</p>

            <p>Thanks,</p>

            <p>Your friendly superintellegent AI website</p>

            <p class="address">120 South Sixth Street, Suite 900, Minneapolis, MN 55402</p>
        </div>
    </div>
</body>
</html>
    HTMLMESSAGE;
    
    // external
    $external_plaintext = '';
    $external_html = '';

    // %internal_name%
    // %contact_name%
    // %contact_email%
    // %contact_number%
    // %contact_company%
    // %contact_existing%
    // %contact_message%