<?php
    class Potential {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function addPotential($data) {
            // Prepare query
            $this->db->query(
                'INSERT INTO potentials
                    (first_name, last_name, email, phone, step_number)
                VALUES
                    (:first_name, :last_name, :email, :phone, :step_number)
                ON DUPLICATE KEY UPDATE
                    first_name = :first_name, 
                    last_name  = :last_name,
                    email      = :email,
                    phone      = :phone,
                    step_number= :step_number'
            );

            // Bind values
            $this->db->bind(':first_name',  $data['first_name']);
            $this->db->bind(':last_name',   $data['last_name']);
            $this->db->bind(':email',       $data['email']);
            $this->db->bind(':phone',       $data['phone']);
            $this->db->bind(':step_number', 1);

            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function addLogo($data) {
            // Prepare query
            $this->db->query(
                'UPDATE potentials SET
                    template_name   = :template_name,
                    template_id     = :template_id,
                    logo_name       = :logo_name,
                    logo_size       = :logo_size,
                    step_number     = 1
                WHERE email = :email AND phone = :phone'
            );

            // Bind values
            $this->db->bind(':template_name',   $data['template_name']);
            $this->db->bind(':template_id',     $data['template_id']);
            $this->db->bind(':logo_name',       $data['logo_name']);
            $this->db->bind(':logo_size',       $data['logo_size']);
            $this->db->bind(':email',           $data['email']);
            $this->db->bind(':phone',           $data['phone']);

            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function getLogo($data) {
            // Prepare query
            $this->db->query(
                "SELECT logo_name FROM potentials WHERE email = :email AND phone = :phone" 
            );

            $this->db->bind(':email', $data['email']);
            $this->db->bind(':phone', $data['phone']);

            $results = $this->db->resultSet();

            return $results;
        }

        public function removeLogo($data) {
            // Prepare query
            $this->db->query(
                'UPDATE potentials SET
                    logo_name       = "",
                    logo_size       = 0
                WHERE email = :email AND phone = :phone'
            );

            // Bind values
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':phone', $data['phone']);

            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function completeStep1($data) {
            // Prepare query
            $this->db->query(
                'UPDATE potentials SET
                    template_name   = :template_name,
                    template_id     = :template_id,
                    logo_url        = :logo_url,
                    need_logo       = :need_logo,
                    business_website= :business_website,
                    youtube_url     = :youtube_url,
                    instagram_url   = :instagram_url,
                    yelp_url        = :yelp_url,
                    facebook_url    = :facebook_url,
                    twitter_url     = :twitter_url,
                    linkedin_url    = :linkedin_url,
                    step_number     = 2
                WHERE email = :email AND phone = :phone'
            );

            // Bind values
            $this->db->bind(':template_name',   $data['template_name']);
            $this->db->bind(':template_id',     $data['template_id']);
            $this->db->bind(':logo_url',        $data['logo_url']);
            $this->db->bind(':need_logo',       $data['need_logo']);
            $this->db->bind(':business_website',$data['business_website']);
            $this->db->bind(':youtube_url',     $data['youtube_url']);
            $this->db->bind(':instagram_url',   $data['instagram_url']);
            $this->db->bind(':yelp_url',        $data['yelp_url']);
            $this->db->bind(':facebook_url',    $data['facebook_url']);
            $this->db->bind(':twitter_url',     $data['twitter_url']);
            $this->db->bind(':linkedin_url',    $data['linkedin_url']);
            $this->db->bind(':email',           $data['email']);
            $this->db->bind(':phone',           $data['phone']);

            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function completeStep2($data) {
            // Prepare query
            $this->db->query(
                'UPDATE potentials SET
                    business_name   = :business_name,
                    business_address= :business_address,
                    business_city   = :business_city,
                    business_state  = :business_state,
                    business_zip    = :business_zip,
                    business_phone  = :business_phone,
                    step_number     = 3
                WHERE email = :email AND phone = :phone'
            );

            // Bind values
            $this->db->bind(':business_name',   $data['business_name']);
            $this->db->bind(':business_address',$data['business_address']);
            $this->db->bind(':business_city',   $data['business_city']);
            $this->db->bind(':business_state',  $data['business_state']);
            $this->db->bind(':business_zip',    $data['business_zip']);
            $this->db->bind(':business_phone',  $data['business_phone']);
            $this->db->bind(':email',           $data['email']);
            $this->db->bind(':phone',           $data['phone']);

            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function getTimePotentials($minutes){
            // Prepare query
            $this->db->query(
                "SELECT * FROM potentials WHERE created_at < DATE_SUB(NOW(), INTERVAL $minutes MINUTE)"
            );

            $results = $this->db->resultSet();
            
            return $results;
        }

        public function getPotential($email) {
            // Prepare query
            $this->db->query(
                "SELECT * FROM potentials WHERE email = :email"
            );

            $this->db->bind(':email', $email);

            $results = $this->db->resultSet();

            return $results;
        }

        public function getAllPotentials() {
            // Prepare query
            $this->db->query(
                'SELECT * FROM potentials ORDER BY created_at DESC'
            );

            $results = $this->db->resultSet();
            
            return $results;
        }
    }