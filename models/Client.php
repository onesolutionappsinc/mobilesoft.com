<?php
    class Client {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function getPotential($email, $phone) {
            // Prepare query
            $this->db->query(
                "SELECT * FROM potentials WHERE email = :email AND phone = :phone"
            );

            $this->db->bind(':email', $email);
            $this->db->bind(':phone', $phone);

            $results = $this->db->resultSet();

            return $results;
        }

        public function removePotential($email, $phone) {
            // Prepare query
            $this->db->query(
                "DELETE FROM potentials WHERE email = :email AND phone = :phone"
            );

            $this->db->bind(':email', $email);
            $this->db->bind(':phone', $phone);

            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }

        public function addClient($data) {
            
            // Prepare query
            $this->db->query(
                'INSERT INTO clients
                    (first_name, last_name, email, phone, business_name, business_address, business_city, business_state, business_zip,
                     business_phone, business_website, industry_id, template_id, template_name, logo_name, logo_size, logo_url, need_logo,
                     youtube_url, instagram_url, yelp_url, facebook_url, twitter_url, linkedin_url, chargebee_id)
                VALUES
                    (:first_name, :last_name, :email, :phone, :business_name, :business_address, :business_city, :business_state, :business_zip,
                     :business_phone, :business_website, :industry_id, :template_id, :template_name, :logo_name, :logo_size, :logo_url, :need_logo,
                     :youtube_url, :instagram_url, :yelp_url, :facebook_url, :twitter_url, :linkedin_url, :chargebee_id)'
            );

            // Bind values
            $this->db->bind(':first_name',          $data[0]->first_name);
            $this->db->bind(':last_name',           $data[0]->last_name);
            $this->db->bind(':email',               $data[0]->email);
            $this->db->bind(':phone',               $data[0]->phone);
            $this->db->bind(':business_name',       $data[0]->business_name);
            $this->db->bind(':business_address',    $data[0]->business_address);
            $this->db->bind(':business_city',       $data[0]->business_city);
            $this->db->bind(':business_state',      $data[0]->business_state);
            $this->db->bind(':business_zip',        $data[0]->business_zip);
            $this->db->bind(':business_phone',      $data[0]->business_phone);
            $this->db->bind(':business_website',    $data[0]->business_website);
            $this->db->bind(':industry_id',         $data[0]->industry_id);
            $this->db->bind(':template_id',         $data[0]->template_id);
            $this->db->bind(':template_name',       $data[0]->template_name);
            $this->db->bind(':logo_name',           $data[0]->logo_name);
            $this->db->bind(':logo_size',           $data[0]->logo_size);
            $this->db->bind(':logo_url',            $data[0]->logo_url);
            $this->db->bind(':need_logo',           $data[0]->need_logo);
            $this->db->bind(':youtube_url',         $data[0]->youtube_url);
            $this->db->bind(':instagram_url',       $data[0]->instagram_url);
            $this->db->bind(':yelp_url',            $data[0]->yelp_url);
            $this->db->bind(':facebook_url',        $data[0]->facebook_url);
            $this->db->bind(':twitter_url',         $data[0]->twitter_url);
            $this->db->bind(':linkedin_url',        $data[0]->linkedin_url);
            $this->db->bind(':chargebee_id',        $data[0]->chargebee_id);
            // Exectute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

    }