<?php
    require_once('../config/variables.php');
    require_once('../lib/pdo_db.php');
    require_once('../models/Potential.php');

    // Sanitize POST Array
    $POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

    $business_name    = $POST['business_name'];
    $business_address = $POST['business_address'];
    $business_city    = $POST['business_city'];
    $business_state   = $POST['business_state'];
    $business_zip     = $POST['business_zip'];
    $business_phone   = $POST['business_phone'];
    $email            = $POST['email'];
    $phone            = $POST['phone'];

    // Potential Customer Data
    $stepData = [
        'business_name'   => $business_name,
        'business_address'=> $business_address,
        'business_city'   => $business_city,
        'business_state'  => $business_state,
        'business_zip'    => $business_zip,
        'business_phone'  => $business_phone,
        'email'           => $email,
        'phone'           => $phone
    ];

    // Instatiate Potential
    $potential = new Potential();

    // Add transaction to database
    $potential->completeStep2($stepData);