<?php

    require_once('../config/variables.php');
    require_once('../lib/pdo_db.php');
    require_once('../models/Potential.php');

    // Sanitize POST Array
    $POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

    $first_name = $POST['first_name'];
    $last_name  = $POST['last_name'];
    $email      = $POST['email'];
    $phone      = $POST['phone'];

    // Potential Customer Data
    $potentialData = [
        'first_name'  => $first_name,
        'last_name'   => $last_name,
        'email'       => $email,
        'phone'       => $phone
    ];

    // Instatiate Potential
    $potential = new Potential();

    // Add transaction to database
    $potential->addPotential($potentialData);