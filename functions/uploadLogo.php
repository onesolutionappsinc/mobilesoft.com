<?php

  header("Content-Type: application/json");
  require_once($_SERVER['DOCUMENT_ROOT'].'/config/variables.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/lib/pdo_db.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/models/Potential.php');

  $returnMessage['error'] = 'none';
  // only allow files uploaded via POST
  if (is_uploaded_file($_FILES['file']['tmp_name'])) {
    // filter email and phone
    $email          = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $phone          = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
    $template_name  = filter_var($_POST['template_name'], FILTER_SANITIZE_STRING);
    $template_id    = filter_var($_POST['template_id'], FILTER_SANITIZE_NUMBER_INT);

    $digits = preg_replace('/\D/', '', $phone);

    // get logo information
    $fileName  = $_FILES['file']['name'];
    $fileType  = $_FILES['file']['type'];
    $fileSize  = $_FILES['file']['size'];
    $fileError = $_FILES['file']['error'];
    $fileExtension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

    // if filesize exceeded then exit
    if ($fileSize > 8388608){
      $returnMessage['status'] = 'failure';
      $returnMessage['error'] = 'Maximum file size exceeded';
      echo json_encode($returnMessage);
      exit();
    }

    // if extension or mime type are invalid exit
    if (!validateImage($fileType, $fileExtension)){
      $returnMessage['status'] = 'failure';
      echo json_encode($returnMessage);
      exit();
    }

    $returnMessage['status'] = 'success';
    $returnMessage['fileName'] = $digits.'-'.str_replace(' ', '_', strtolower($fileName));
    $returnMessage['fileSize'] = $fileSize;

    // move file to temporary folder and add entry to database.
    $name = basename($fileName);
    $location = $_SERVER['DOCUMENT_ROOT'].'/incomplete/'.$digits.'-'.str_replace(' ', '_', strtolower($name));
    $sourcePath = $_FILES['file']['tmp_name'];
    move_uploaded_file($sourcePath, $location);

    // logo step data
    $data = [
      'email'         => $email,
      'phone'         => $phone,
      'template_name' => $template_name,
      'template_id'   => $template_id,
      'logo_name'     => $digits.'-'.str_replace(' ', '_', strtolower($fileName)),
      'logo_size'     => $fileSize
    ];

    // Instatiate Potential
    $potential = new Potential();

    // Add transaction to database
    $potential->addLogo($data);

    echo json_encode($returnMessage);
  } else {
    $returnMessage['error'] = 'not an uploaded file';
    echo json_encode($returnMessage);
    exit();
  }

  function validateImage($mime, $extension){
    
    global $returnMessage;

    // check extension
    $validExtensions = array('ai','bmp','gif','jpeg','jpg','pdf','png','psd','svg','tiff','tif','webp');
    if (!in_array($extension, $validExtensions)){
      $returnMessage['error'] = 'Invalid file type';
      return false;
    }
    
    // check frontend mime type
    if (!((explode('/',$mime)[0] == 'image')              // image
          || ($mime == 'application/postscript')          // ai
          || ($mime == 'application/pdf')                 // pdf
          || ($mime == 'application/octet-stream')        // photoshop
          || ($mime == 'application/x-photoshop')         // photoshop
          || ($mime == 'application/photoshop')           // photoshop
          || ($mime == 'application/psd')                 // photoshop
          || ($mime == 'zz-application/zz-winassoc-psd')  // photoshop        
          ) // psd
    ){
      $returnMessage['error'] = 'Invalid file type';
      return false;
    }

    // verify that PHP mime type matches frontend mime type
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $phpMime = finfo_file($finfo,$_FILES['file']['tmp_name']);
    finfo_close($finfo);
    $validPSD = array('image/psd','image/x-photoshop','application/photoshop','zz-application/zz-winassoc-psd','application/psd','image/vnd.adobe.photoshop');
    if (!(($phpMime == $mime)
        || ($phpMime == 'application/pdf' && $mime == 'application/postscript')
        || ($phpMime == 'image/x-ms-bmp' && $mime == 'image/bmp')
        || in_array($phpMime, $validPSD)) // there are too many valid PSD mime types.  Just check if it is a PSD.
    ){
      $returnMessage['error'] = 'Invalid file type: '.$phpMime.' | '.$mime;
      return false;
    }
    return true;
  }