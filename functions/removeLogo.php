<?php
  header("Content-Type: application/json");
  require_once($_SERVER['DOCUMENT_ROOT'].'/config/variables.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/lib/pdo_db.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/models/Potential.php');

  $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
  $phone = filter_var($_POST['phone'], FILTER_SANITIZE_STRING); 

  // logo step data
  $data = [
    'email' => $email,
    'phone' => $phone
  ];

  // Instatiate Potential
  $potential = new Potential();

  $logoName = $potential->getLogo($data)[0]->logo_name;

  // Remove logo from database
  $potential->removeLogo($data);

  // Remove file from folder
  if ($logoName != '') unlink($_SERVER['DOCUMENT_ROOT'].'/incomplete/'.$logoName);
  
  $returnMessage['status'] = 'success';
  echo json_encode($returnMessage);