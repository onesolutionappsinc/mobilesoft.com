<?php
    require_once('../config/variables.php');
    require_once('../lib/pdo_db.php');
    require_once('../models/Potential.php');

    // Sanitize POST Array
    $POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

    $template_name    = $POST['template_name'];
    $template_id      = $POST['template_id'];
    $logo_url         = $POST['logo_url'];
    $need_logo        = $POST['need_logo'];
    $business_website = $POST['business_website'];
    $youtube_url      = $POST['youtube_url'];
    $instagram_url    = $POST['instagram_url'];
    $yelp_url         = $POST['yelp_url'];
    $facebook_url     = $POST['facebook_url'];
    $twitter_url      = $POST['twitter_url'];
    $linkedin_url     = $POST['linkedin_url'];
    $email            = $POST['email'];
    $phone            = $POST['phone'];

    // Potential Customer Data
    $stepData = [
        'template_name'   => $template_name,
        'template_id'     => $template_id,
        'logo_url'        => $logo_url,
        'need_logo'       => $need_logo,
        'business_website'=> $business_website,
        'youtube_url'     => $youtube_url,
        'instagram_url'   => $instagram_url,
        'yelp_url'        => $yelp_url,
        'facebook_url'    => $facebook_url,
        'twitter_url'     => $twitter_url,
        'linkedin_url'    => $linkedin_url,
        'email'           => $email,
        'phone'           => $phone
    ];

    // Instatiate Potential
    $potential = new Potential();

    // Add transaction to database
    $potential->completeStep1($stepData);