<?php
  header("Content-Type: application/json");
  require_once('../config/variables.php');
  require_once('../lib/pdo_db.php');
  require_once('../models/Potential.php');

  // Sanitize POST Array
  $POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

  $email = $POST['email'];

  // Instantiate Potential
  $potential = new Potential();

  // Get Customer
  $potential_data = $potential->getPotential($email);

  // Return only needed data
  // -- not found
  if (empty($potential_data[0])){
    $data = array('status' => 'failure');
  } else {
    // -- completed welcome step
    if ($potential_data[0]->step_number >= 0){
      $data = array(
        'status' => 'success',
        'first-name'        => $potential_data[0]->first_name,
        'last-name'         => $potential_data[0]->last_name,
        'email'             => $potential_data[0]->email,
        'phone'             => $potential_data[0]->phone,
        'step_number'       => $potential_data[0]->step_number
      );
    }
    if ($potential_data[0]->step_number >= 1){
      $step1 = array(
        'template-id'       => $potential_data[0]->template_id,
        'template-name'     => $potential_data[0]->template_name,
        'logo_name'         => $potential_data[0]->logo_name,
        'logo_size'         => $potential_data[0]->logo_size,
        'need_logo'         => $potential_data[0]->need_logo,
        'business_website'  => $potential_data[0]->business_website,
        'youtube_url'       => $potential_data[0]->youtube_url,
        'instagram_url'     => $potential_data[0]->instagram_url,
        'yelp_url'          => $potential_data[0]->yelp_url,
        'facebook_url'      => $potential_data[0]->facebook_url,
        'twitter_url'       => $potential_data[0]->twitter_url,
        'linkedin_url'      => $potential_data[0]->linkedin_url
      );
      $data = array_merge($data, $step1);
    }
    if ($potential_data[0]->step_number >= 2){
      $step2 = array(
        'business_name'     => $potential_data[0]->business_name,
        'business_address'  => $potential_data[0]->business_address,
        'business_city'     => $potential_data[0]->business_city,
        'business_state'    => $potential_data[0]->business_state,
        'business_zip'      => $potential_data[0]->business_zip,
        'business_phone'    => $potential_data[0]->business_phone,
      );
      $data = array_merge($data, $step2);
    }
  }

  // return data
  echo json_encode($data);