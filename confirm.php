<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Confirmation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/vendor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styles/vendor/slick.css" />
    <link rel="stylesheet" type="text/css" href="styles/vendor/slick-theme.css" />
    <link rel="stylesheet" href="styles/vendor/fontawesome.min.css">
    <link rel="stylesheet" href="styles/fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="styles/styles.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="styles/desktop.css" />
    <script src="js/main.js"></script>
</head>

<body>
    <?php include_once('includes/header.php'); ?>
    <div class="container-fluid content">
        <div class="container ">
            <div class="white-box confirmation-box moveDown text-center">
                <div class="row justify-content-center">
                    <img class="image-fluid col-2" src="img/confirmation-icon.png" alt="">
                    <br>
                    <h1 class="col-12 light-blue">Thank you for your submission</h1>
                    <h6 class="col-8 bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor
                        incididunt
                        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris
                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                        velit
                        esse
                        cillum dolore</h6>
                    <p class="col-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore
                        et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                        aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                        deserunt mollit anim id est laborum.</p>
                </div>
                <hr class="hr-text" data-content="Some Helpful Links to Checkout">
                <div class="confirm-link-container ">
                    <div class="row text-center justify-content-center">
                        <div class="col-2 light-blue-bg rounded-left shadow"><img class="img-fluid"
                                src="img/blog-icon.png" alt="">
                        </div>
                        <div class="col-7 white-bg rounded-right shadow">
                            <h4>Add on Features</h4>
                            <p>View our add on feature</p>
                        </div>
                    </div>
                    <div class="row text-center justify-content-center">
                        <div class="col-2 light-blue-bg rounded-left shadow"><img class="img-fluid"
                                src="img/blog-icon.png" alt="">
                        </div>
                        <div class="col-7 white-bg rounded-right shadow">
                            <h4>Add on Features</h4>
                            <p>View our add on feature</p>
                        </div>
                    </div>
                    <div class="row text-center justify-content-center">
                        <div class="col-2 light-blue-bg rounded-left shadow"><img class="img-fluid"
                                src="img/blog-icon.png" alt="">
                        </div>
                        <div class="col-7 white-bg rounded-right shadow">
                            <h4>Add on Features</h4>
                            <p>View our add on feature</p>
                        </div>
                    </div>
                </div>
                <div class="container-fluid light-blue-bg text-center">
                    <h3 class="white">Like, Share, and Follow us on Social Media</h3>
                    <div class="row justify-content-center ">
                        <a  class="col-md-3 col-sm-1" href="#"><img src="img/youtube-icon.png" alt=""></a>
                        <a  class="col-md-3 col-sm-1" href="#"><img src="img/youtube-icon.png" alt=""></a>
                        <a class="col-md-3 col-sm-1" href="#"><img src="img/youtube-icon.png" alt=""></a>
                        <a class="col-md-3 col-sm-1" href="#"><img src="img/youtube-icon.png" alt=""></a>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid white-bg text-center">
        <div class="confirm-banner">
            <h5 class="light-grey">300,000 new smartphones are activated everyday. Isn't it time you got in the game?</h5><br>
            <p class="bold black">CONTACT MOBILESOFT TODAY</p> <br>
            <button onclick="orderLink()" class="btn btn-secondary blurple-bg center">CONTACT US</button>
            </div>
        </div>

    </div>




    <?php include_once('includes/footer.php'); ?>
    <script src="js/vendor/jquery-3.3.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
</body>

</html>